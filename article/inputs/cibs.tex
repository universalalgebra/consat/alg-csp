
Taylor terms were defined in Section~\ref{ssec:term-ops}. It is not hard to see that a binary 
term is Taylor if and only if it is idempotent and commutative. This suggests, in light of the 
algebraic \csp-dichotomy conjecture, that we study commutative, idempotent binars (\cib's for 
short), that is, algebras with a single basic binary operation that is commutative and 
idempotent. If the conjecture is true, then every finite \cib should be tractable. 

The associative {\cib}s are precisely the semilattices. Finite semilattices have long been known to be tractable, 
\cite{MR1481313}. The variety of semilattices is \sdm and is equationally complete. Every nontrivial semilattice 
must contain a subalgebra isomorphic to the unique two-element semilattice, $\slt=\<\{0,1\},\meet\>$, and conversely, \slt\ generates the variety of all semilattices.

As we shall see, the algebra \slt plays a central role in the structure of {\cib}s. In particular, the omission of \slt implies tractability. 

\begin{theorem}
  \label{thm:omit5-cib}
  If $\bA$ is a finite \cib then the following are equivalent:
  \begin{enumerate}[(i)] %[label=(\arabic*)]
  \item $\bA$ has an edge term.
  \item $\var{V}(\bA)$ is congruence modular.
  \item $\slt \notin \sansH \sansS (\bA)$ 
    \end{enumerate}
\end{theorem}
The proof that (1) implies (2) appears in \cite{MR2563736} and holds for general
finite idempotent algebras. The contrapositive of (2) implies (3) is easy: 
if 
$\slt \in \sansH \sansS (\bA)$, then 
$\slt^2 \in \var{V}(\bA)$, and the congruence
lattice of $\slt^2$ is not modular.
So it remains to show that (3) implies (1). For this we use the idea of a 
``cube-term blocker'' (\cite{MR2926316}).
  A \emph{cube term blocker} (\ctb) for an algebra $\bA$ is a pair $(D, S)$ of subuniverses
  with the following properties: $\emptyset < D < S\leq A$ and for every term 
  $t(x_0, x_1, \dots, x_{n-1})$ of $\bA$ there is an index $i \in \nn$ such that, 
  for all $\bs = (s_0, s_1, \dots, s_{n-1}) \in S^n$, if $s_i \in D$ 
  then $t(\bs)\in D$.


\begin{theorem}[\protect{\cite[Thm 2.1]{MR2926316}}]
\label{thm:ctb}
Let $\bA$ be a finite idempotent algebra. Then $\bA$ has an edge term iff
it possesses no cube-term blockers.
\end{theorem}

(The notions of cube term and edge term both originate in~\cite{MR2563736}. In that paper it is proved that a finite algebra has a cube term if and only if it has an edge term.)

\begin{lemma}
\label{lem:cib-ctb}
  A finite \cib $\bA$ has a \ctb if and only if $\slt \in \sansH \sansS (\bA)$.
\end{lemma}
\begin{proof}
  Assume $(D, S)$ is a \ctb for $\bA$. Then there exists $s\in S-D$.  Consider
  $D^+ :=D \cup\{s\}$.  Evidently  $D^+$ is a subuniverse of $\bA$, and 
  if $\bD^+$ denotes the corresponding subalgebra, then 
  $\theta_D := D^2 \cup \{(s,s)\}$ is a congruence of $\bD^+$. It's easy to see that
  $\bD^+/\theta_D \cong \slt$, so $\slt \in \sansH \sansS (\bA)$.
  
  Conversely, if $\slt \in \sansH \sansS (\bA)$, then there exists $\bB \leq \bA$ and
  a surjective homomorphism $h\colon \bB \to \slt$. Let $B_0=h^{-1}(0)$. Then $\emptyset \neq B_0 < B\leq A$, and $(B_0, B)$ is a \ctb for $\bA$.  
  \end{proof}
  
Lemma~\ref{lem:cib-ctb}, along with Theorem~\ref{thm:ctb}, completes the proof of
Theorem~\ref{thm:omit5-cib} by showing (1) is false if and only if (3) is false. 
In fact, something stronger is true. Kearnes has shown that if $\var{V}$ is any variety of {\cib}s that omits \slt, then $\var{V}$ is congruence permutable. 

\begin{corollary}\label{cor:edge-prod}
Let $\bA_0$, $\bA_1$,\dots,$\bA_{n-1}$ be finite {\cib}s satisfying the equivalent conditions in Theorem~\ref{thm:omit5-cib}. Then both $\bA_0\times \cdots\times \bA_{n-1}$ and $\bA_0\times\cdots\times \bA_{n-1} \times \slt$ are tractable.
\end{corollary}

\begin{proof}
By the theorem, $\var{V}(\bA_0)$ and $\var{V}(\bA_1)$ each have an edge term. By Theorem~\ref{thm:robust}, $\sansH\bigl(\var{V}(\bA_0)\circ \var{V}(\bA_1)\bigr)$ has an edge term as well. Since $\bA_0\times \bA_1$ lies in this variety, it possesses that same edge term. Iterating this process, the algebra $\bB=\bA_0 \times \cdots \times \bA_{n-1}$ has an edge term, hence is tractable by Theorem~\ref{thm:edge-tractable}.

Let $t(x_1,\dots,x_{k+1})$ be an edge term for $\bB$. Then according to the first identity in Definition~\ref{defn:edge-term}, $\var{V}(\bB)$ is a strongly irregular variety. Consequently, the algebra $\bB\times \slt$ is a P\l onka sum of two copies of \bB. As a P\l onka sum of tractable algebras, Theorem~4.1 of~\cite{MR3350338} implies that $\bB\times \slt$ is tractable as well.
\end{proof}

It follows from the corollary and Theorem~\ref{thm:HSP-tract} that every finite member of $\var{V}(\bA_0\times\bA_1\times\cdots\times \bA_{n-1}\times \slt)$ is at least locally tractable. 

As a counterpoint to Theorem~\ref{thm:omit5-cib}, we mention the following. The proof requires some basic knowledge of tame congruence theory.

\begin{theorem}\label{thm:no-abelians}
Let \bA\ be a finite \cib\ and suppose that $\sansH\sansS(\bA)$ contains no nontrivial abelian algebras. Then $\var{V}(\bA)$ is \sdm. Consequently, \bA\ is a tractable algebra.
\end{theorem}

\begin{proof}
Since every \cib\ has a Taylor term, and since $\sansH\sansS(\bA)$ has no abelian algebras, $\sansS(\bA)$ omits types $\{1,2\}$. Then by \cite[Cor~2.2]{Freese:2009}, $\var{V}(\bA)$ omits types $\{1,2\}$. But then, by \cite[Thm~9.10]{HM:1988}, $\var{V}(\bA)$ is \sdm.
\end{proof}

In a congruence-permutable variety, an algebra is abelian if and only if it is polynomially equivalent to a faithful, unital module over a ring. See~\cite[Thm~7.35]{MR2839398} for a full discussion. For example, consider the ring $\mathbb Z_3$ as a module over itself. Define the binary polynomial $x\cdot y = 2x+2y$ on this module. The table for this operation is given at the left of Figure~\ref{fig:cib3}. Conversely, we can retrieve the module operations from `$\cdot$' by $x+y = 0\cdot(x\cdot y)$ and $2x=0\cdot x$. Consequently, the commutative idempotent binar $\Sq3$ is abelian. 

In light of Theorem~\ref{thm:type2cp}, every finite, abelian \cib\ will be of this form. We make the following observation. 

\begin{proposition}\label{prop:cib_ab_odd}
A finite, abelian \cib\ has odd order.
\end{proposition}

\begin{proof}
Let \bA\ be a finite, abelian \cib. By Theorem~\ref{thm:type2cp} (since every \cib is Taylor), \bA\ is  polynomially equivalent to a faithful, unital module $M$ over a ring $R$.  The basic operation of $\bA$ must be a polynomial of $M$. Thus there are $r,s\in R$ and $b\in M$ such that $x\cdot y = rx + sy +b$. Let 0 denote the zero element of $M$. Then $0=0\cdot 0 = r0 +s0 +b =b$. The condition $x\cdot y = y \cdot x$ implies  $rx+sy = ry+sx$, so (by taking $y=0$ and since $M$ is faithful), $r=s$. Finally by idempotence, $2r=1$. 

Now, if $A$, hence $M$, has even cardinality, there is an element $a\in A$, $a\neq 0$, of additive order~2. Thus
\begin{equation*}
a= a\cdot a = 2ra = 0
\end{equation*}
which is a contradiction.
\end{proof}

In an effort to demonstrate the utility of the techniques developed in this paper, we shall now show that every \cib of cardinality at most~4 is tractable. Of course, it is known that the algebraic dichotomy conjecture holds for every idempotent algebra of cardinality at most~3~\cite{MR2212000, MR521057}. However, our arguments are relatively short and may indicate why {\cib}s may be more manageable than arbitrary algebras.

For the remainder of this section, let \bA\ be a \cib with universe $\nn$. We shall consider the various possibilities for $n$ and \bA\ (up to isomorphism), and in each case show that it is tractable. In order to make it clear that our inventory is complete, a few of the arguments and computations are postponed until the end.

\casespec{$\bm{n=2}$} It is easy to see that there is a unique 2-element \cib, namely \slt. Idempotence determines the diagonal entries in the table, and by commutativity, the remaining two entries must be equal. The choices $0$ and $1$ for the off-diagonal yield a meet-semilattice and a join-semilattice respectively. As we remarked above, every finite semilattice is tractable, and, in fact, $\Sl$, the variety of semilattices, is \sdm. 

\casespec{$\bm{n=3}$, \bA\ not simple} There is a congruence, $\theta$, on \bA\ with $0_A < \theta < 1_A$. It is important to note that by idempotence, every congruence class is a subalgebra. Based purely on cardinality concerns, $\bA/\theta$ has cardinality~2, one $\theta$ class has 2 elements, the other has~1. From the uniqueness of the 2-element algebra, $\bA/\theta$ and each of the $\theta$-classes are semilattices. Consequently, $\bA \in \Sl \circ \Sl$. By Theorem~\ref{thm:robust}, $\Sl\circ \Sl$ is \sdm, so by Theorem~\ref{thm:sdm-tractable}, \bA\ is tractable. 

\begin{figure}
\centering
\begin{tabular}{ccc}
  \begin{tabular}{c|ccc}
   $\cdot$&0&1&2\\
  \hline
  0&0&2&1\\
  1&2&1&0\\
  2&1&0&2
  \end{tabular} &
  %
  \begin{tabular}{c|ccc}
  $\cdot$&0&1&2\\
  \hline
  0&0&0&1\\
  1&0&1&2\\
  2&1&2&2
  \end{tabular} &
  %
  \begin{tabular}{c|ccc}
  $\cdot$&0&1&2\\
  \hline
  0&0&0&2\\
  1&0&1&1\\
  2&2&1&2
  \end{tabular} \\
  %
  \vrule height 12pt width 0pt $\Sq3$ & $\bT_1$ & $\bT_2$
 \end{tabular}
 \caption{The simple {\cib}s of cardinality 3}\label{fig:cib3}
 \end{figure}

\casespec{$\bm{n=3}$, \bA\ simple} On the other hand, suppose that \bA\ is simple. If \bA\ has no proper nontrivial subalgebras, then in \bA, $x\neq y \implies x\cdot y \notin \{x,y\}$. It follows that \bA\ must be the 3-element Steiner quasigroup, $\Sq3$, of Figure~\ref{fig:cib3}. By Corollary~\ref{cor:edge-prod}, \bA\ is tractable. 

Finally, if \bA\ has a proper nontrivial subalgebra, that subalgebra must be isomorphic to \slt. Thus no nontrivial subalgebra of \bA\ is abelian. By Theorem~\ref{thm:no-abelians}, \bA\ generates an \sdm variety, so is tractable by Theorem~\ref{thm:sdm-tractable}. For future reference, there are 2 algebras meeting the description in this paragraph, $\bT_1$ and $\bT_2$. (This is easy to check by hand.) Their tables are given in Figure~\ref{fig:cib3}.

\casespec{$\bm{n=4}$, \bA\ not simple} Let $\theta$ be a maximal congruence of \bA. Then $\bA/\theta$ is simple, so from the previous few paragraphs, $\bA/\theta$ is isomorphic to one of $\bT_1$, $\bT_2$, $\Sq3$, or $\slt$. If $\bA/\theta \cong \bT_i$, then the $\theta$-classes must have size $1,1,2$. Consequently, each $\theta$-class is a semilattice, so $\bA\in \Sl \circ \var{V}(\bT_i)$ which is \sdm by the computations above and Theorem~\ref{thm:robust}. Thus $\bA$ is tractable.

The next case to consider is $\bA/\theta \cong \Sq3$. Without loss of generality, assume that~$\theta$ partitions the universe as $|01|2|3|$, and further that $0\cdot 1=0$. From this data we deduce that the operation table for \bA\ must be
\begin{equation*}
\begin{tabular}{c|cccc}
      $\cdot $ & 0 & 1 & 2 & 3\\
      \hline
      0 & 0 & 0 & 3& 2\\
      1 & 0 & 1 & 3& 2\\
      2 & 3 & 3 & 2 & $a$\\
      3 & 2 & 2 & $a$ & 3
 \end{tabular}\qquad \text{with $a\in \{0,1\}$.}
\end{equation*}
If $a=0$, then the only way to obtain 1 as a product is $1\cdot1$. In that case there is a homomorphism onto \slt with kernel $\psi=|023|1|$. We have $\theta \cap \psi = 0_A$, so $\bA$ is a subdirect product of $\Sq3 \times \slt$. Therefore, by \ref{cor:edge-prod}, \bA\ is tractable. 

More problematic is the case $a=1$. We established tractability of that algebra in Example~\ref{ex:quot-inst}.

Finally, suppose that $\bA/\theta \cong \slt$. The possible sizes of the $\theta$-classes are 3,1 or 2,2. If neither class is isomorphic to \Sq3 then the classes both lie in an \sdm variety, so by 
Theorem~\ref{thm:robust}, \bA\ too lies in an \sdm variety, and is tractable. On the other hand, suppose that one of the classes is isomorphic to \Sq3, (there are 7 such algebras). Then \bA\ is an EC algebra (with $t(x,y)=y\cdot(x\cdot y)$) and we can apply Theorem~\ref{thm:least-block}
to establish that \bA\ is tractable. 

  
\casespec{$\bm{n=4}$, \bA\ simple} By Theorem~\ref{thm:no-abelians}, if $\sansH\sansS(\bA)$ contains no nontrivial abelian algebra, then \bA\ is tractable. Thus, we may as well assume that $\sansH\sansS(\bA)$ contains an abelian algebra. By Proposition~\ref{prop:cib_ab_odd}, \bA\ itself is nonabelian. Consequently (since \bA\ is simple) \bA\ must have a subalgebra isomorphic to \Sq3. Without loss of generality, let us assume that $\{1,2,3\}$ forms this subalgebra.

Similarly, by Corollary~\ref{cor:edge-prod}, we can assume that $\sansH\sansS(\bA)$ contains a copy of \slt. Examining the 2- and 3-element \cibs, it is easy to see that if $\slt\in \sansH\sansS(\bA)$, then already $\slt\in\sansS(\bA)$. By the symmetry of \Sq3, and the fact that it contains no semilattice, we can assume that $\{0,1\}$ forms the semilattice.


 Thus, the table for \bA\ must have one of the following two forms.
\begin{equation*}
\begin{tabular}{c|cccc}
$\cdot$&0&1&2&3\\
\hline
0&0&0&$u_2$&$u_3$\\
1&0&1&3&2\\
2&$u_2$&3&2&1\\
3&$u_3$&2&1&3
\end{tabular} 
\qquad
\begin{tabular}{c|cccc}
$\cdot$&0&1&2&3\\
\hline
0&0&1&$v_2$&$v_3$\\
1&1&1&3&2\\
2&$v_2$&3&2&1\\
3&$v_3$&2&1&3
\end{tabular} 
\end{equation*}
By checking the 32 possible tables, either via the universal algebra calculator~\cite{UAcalc} or directly using Freese's algorithm~\cite{Freese2008}, one determines that there are 7 pairwise nonisomorphic algebras of one of these two forms. Interestingly, every simple algebra of the second form is isomorphic to one of the first form. Thus we will use the first form for all the candidates. These 7 algebras are indicated in Figure~\ref{fig:simple7}.

\begin{figure}
\centering
\begin{tabular}{l|cc|l}
&$u_2$&$u_3$&Proper Nontrivial Subalgebras\\
\hline
$\bA_0$&0&1&$\{0,1\},\, \{0,2\},\, \{1,2,3\}$\\
$\bA_1$&1&1&$\{0,1\},\, \{1,2,3\}$\\
$\bA_2$&1&2&$\{0,1\},\, \{1,2,3\}$\\
$\bA_3$&0&3&$\{0,1\},\, \{0,2\},\, \{0,3\},\, \{1,2,3\}$\\
$\bA_4$&1&3&$\{0,1\},\, \{0,3\},\, \{1,2,3\}$\\
$\bA_5$&2&2&$\{0,1\},\, \{0,2\},\, \{0,3\}\, \{1,2,3\}$\\
$\bA_6$&2&3&$\{0,1\},\, \{0,2\},\, \{0,3\}\, \{1,2,3\}$
\end{tabular}
 \caption{7 simple \cibs of size 4.}\label{fig:simple7}
 \end{figure}
 
Our intent is to apply the rectangularity theorem to establish the tractability of these~7 algebras. For this, we need to determine the minimal absorbing subuniverses of each subalgebra of \bA. First observe that if $\{a,b\}$ forms a copy of \slt\ with $a\cdot b =a$, then $\{a\} \absorbing_t \{a,b\}$, with $t(x,y)=x\cdot y$, while $\{b\}$ is not absorbing since $a$ is a sink (Lemma~\ref{lem:sink}). Second, by Lemma~\ref{lem:abelian-AF}, \Sq3 is absorption-free, which is to say, it is its own minimal absorbing subalgebra. 

 Now let $B$ be a proper minimal absorbing subalgebras of $\bA_i$, for $i<7$. Then $B\cap \{1,2,3\}$ is absorbing in $\{1,2,3\}$ (Lemma~\ref{lem:restriction}). As $\{1,2,3\}\cong \Sq3$ is absorption free and $B$ is a proper subalgebra of \bA, we must have $B=\{1,2,3\}$ or $B=\{0\}$. However the first of these is impossible because $\{1,2,3\} \cap \{0,1\} = \{1\}$, which is not absorbing in $\{0,1\}$ since $0$ is a sink. Thus the only possible absorbing subalgebra of \bA\ is $\{0\}$. 
 
 For $i\leq 2$, $\{0\}$ is indeed absorbing in $\bA_i$ with binary absorbing term $t(x,y)=\bigl(x(xy)\bigr)\bigl(y(xy)\bigr)$. However, observe that if $i>2$ then according to Figure~\ref{fig:simple7} either $u_2=2$ or $u_3=3$. If $u_2=2$ then $2$ is a sink for the subuniverse $\{0,2\}$. In that case $\{0\}= B\cap \{0,2\}$ is not absorbing in $\{0,2\}$ contradicting the fact that $B$ is absorbing in \bA. A similar argument works for $u_3=3$ and $\{0,3\}$. Thus $\bA_i$ is absorption free for $i>2$. 
 
 We summarize these computations in the following table.
 \begin{center}
 \begin{tabular}{lc}
 Algebra&Minimal Absorbing Subalgebra\\
 $\slt=\{0,1\}$ & $\{0\}$ \\
 $\{1,2,3\}$ & $\{1,2,3\}$ \\
 $\bA_0, \bA_1, \bA_2$ & $\{0\}$\\ 
 $\bA_3,\dots,\bA_6$ & $A$
 \end{tabular}
 \end{center}
 The crucial point here is that every member of $\sansS(\bA_i)$ has a unique minimal absorbing subalgebra. 
 
We now proceed to argue that each algebra from Figure~\ref{fig:simple7} is tractable. Following Definition~\ref{def:csp}, let $\sI=\<\sV, \sA, \sS, \sR\>$ be an instance of $\CSP(\bA_i)$, for some $i<7$. We have $\sA=(\bC_0, \bC_1,\dots,\bC_{n-1})$, in which each $\bC_i$ is a subalgebra of $\bA_i$, and $\sR=(R_0,\dots,R_{J-1})$ is a list of subdirect products of the various $\bC_k$'s. We must show that there is an algorithm that determines, in polynomial time, whether this instance has a solution. For this we will use Corollary~\ref{cor:RT-cor-gen}.

For each $j<J$, let the relation $\bar R_j$ be obtained from $R_j$ as follows. First, for any distinct variables $\ell,k \in \im(\sigma_j)$, if $\etaR^j_\ell = \etaR^j_k$,  remove the $k$-th variable from the scope of $R_j$. Then, define $\bar R_j = R_j \times \prod_{\ell \notin \im\sigma_j} C_i$. Let $\bar \sI$ denote the modified instance with $(\bar R_j : j<J)$ replacing $\sR$ and every scope equal to $\sV$. Then the solution set of $\bar \sI$ is identical to the solution set of the original instance $\sI$, and, in fact, is equal to $\bigcap_j \bar R_j$. 

Dropping variable $k$ does not affect the solution set: if $\etaR^\ell = \etaR^k$ then there is an isomorphism $h\colon \bC_\ell \to \bC_k$ such that $h\circ \Proj_\ell = \Proj_k$. Thus, the $k$-th coordinate of any tuple in $R$ can be recovered by applying $h$ to the $\ell$-th coordinate. The second step in the transformation clearly does not lose any of the original solutions. Furthermore, it is easy to see that it does not create any new pairs $\ell,k$ with $\etaR_\ell = \etaR_k$. We stress that the creation of $\bar R$ from $R$ is not part of the algorithm, so it is not necessary to execute in polynomial-time. It exists only for the purpose of this proof. 

We now consider the 5 conditions in Corollary~\ref{cor:RT-cor-gen}, applied to $\{\,\bar R_j : j<J\,\}$. Set $\alpha=\{\,\ell<n : \bC_\ell \cong \Sq3\,\}$. Since \Sq3 is the only possible abelian subalgebra of $\bA_i$, the first two conditions will be satisfied. Our preprocessing ensures that condition~3 holds as well. We argue momentarily that condition~\ref{item:RT-cor-gen-4} holds. Assuming for the moment that it does, then according to the corollary and our computations above, the following statements are equivalent.
\begin{itemize}
\item $\sI$ has a solution;
\item $\bar \sI$ has a solution;
\item $\bigcap_j \bar R_j \neq \emptyset$;
\item $\bigcap_j \Proj_\alpha \bar R_j \neq \emptyset$;
\item $\sI_\alpha$ has a solution.
\end{itemize}
However, $\sI_\alpha$ is an instance of $\CSP(\Sq3)$, which, as an abelian algebra, is known to be tractable. Thus by running the algorithm on this partial instance, we can determine the existence of a solution to the original instance, $\sI$.

Thus, we are left with the task of verifying condition~\ref{item:RT-cor-gen-4}, that is, show that every $\bar R_j$ intersects $\prod B_\ell$. Recall that $B_\ell$ is a minimal absorbing subalgebra of $\bC_\ell$. We have two cases to consider. First, suppose that $i<3$. In that case the minimal absorbing subalgebra of $\bA_i$ is $\{0\}$, which is, in fact, a sink of $\bA_i$. Therefore, for every $\ell \in \alpha$, $B_\ell = \{1,2,3\}= C_\ell$, while for $\ell \notin \alpha$, $B_\ell = \{0\}$. Let $\bar R$ denote any $\bar R_j$. Since $\bar R$ is subdirect, for every $\ell \in \alpha'$ there is a tuple $\br^\ell \in R$ with $r^\ell_\ell = 0$. Let $\br$ be the product of all the $\br^\ell$'s (in any order). Then, since $0$ is a sink, $\br_\ell = 0\in B_\ell$ for every $\ell \in \alpha'$, while for $\ell \in \alpha$, $\br_\ell \in \bC_\ell = B_\ell$. Thus the condition is satisfied.

The argument when $i\geq 3$ is not very different. In this case, let $\beta = \{\,\ell <n : \bC_\ell = \slt\,\}$. Once again, for $\ell \in \beta$ let $\br^\ell$ be a tuple with $\br^\ell_\ell = 0$, and let $\br$ be the product. Then for $\ell \in \beta$, $\br_\ell = 0 \in B_\ell$, while, for $\ell \notin \beta$, $\br_\ell \in B_\ell$ since $B_\ell = \{1,2,3\} = \bC_\ell$ (if $\ell \in \alpha$) and $B_\ell = A_i$ otherwise. 

