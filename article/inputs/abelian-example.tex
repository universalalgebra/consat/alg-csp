
The example in this section reveals why the rectangularity theorem
cannot be generalized to products of abelian algebras.
Before examining the example, we state a lemma that will be useful
when discussing the example. The proof is straightforward.
\begin{lemma}
\label{lem:abs2}
Let $\bA_1, \dots, \bA_n$ be finite simple algebras in a Taylor
variety and suppose
\begin{itemize}
\item each $\bA_i$ is absorption-free, 
\item $\bR \sdp \bA_1 \times \cdots \times \bA_n$,
\item $\etaR_i \neq \etaR_j$ for all $i\neq j$, and 
\item $\mu \subseteq \nn $ is minimal among the sets in
$\{\sigma \subseteq \nn  \mid \bigwedge_{\sigma} \etaR_i = 0_R\}$.
\end{itemize}
Then, $|\mu|>1$ and $\Proj_{\tau} \bR = \Pi_\tau \bA_i$ for every set
$\tau \subseteq \nn $ with $1< |\tau|\leq |\mu|$.
\end{lemma}


\begin{example}
  Let $\bA$ be the algebra $\<\{0,1\}, \{f\}\>$, with universe $\{0,1\}$,
  and a single basic operation given by the ternary function $f(x,y,z) = x+y+z$
  where addition is modulo 2.  This algebra is clearly simple and has two proper
  subuniverses, $\{0\}$ and $\{1\}$, neither of which is absorbing, so $\bA$ is
  absorption-free.  Let $\bR \sdp \bA \times \bA \times \bA$ be the subdirect
  power of $\bA$ with universe 
  \[ R = \{(x,y,z)\in A^3\mid x+y+z=0 \text{ (mod $2$)}\} = \{(0,0,0), (1,1,0), (1,0,1), (0,1,1) \}.\] 
  It's convenient to give short names to the elements of $R$.  
  Let us use the integers that they (as binary tuples) represent.
  That is, $0 = (0,0,0)$, $3 = (1,1,0)$,  $5 = (1,0,1)$,  and 
  $6 = (0,1,1)$,   
  so $R = \{0, 3, 5, 6\}$.
  Let $\etaR_i = \ker(\bR \onto \bA_i)$, and identify each
  congruence with the associated partition of the set $R$. Then,
  $\etaR_1 = |0,6|3,5|; \,  \etaR_2 = |0,5|3,6|; \,  \etaR_3 = |0,3|5,6|$,
  so $\etaR_i \meet \etaR_j = 0_R$ and $\etaR_i \join \etaR_j = 1_R$.
  In fact, the three projection kernels are the only nontrivial congruence
  relations of $\bR$.

  \tikzstyle{lat} = [circle,draw,inner sep=0.8pt]
  \begin{center}
  \begin{tikzpicture}[scale=1.2]
    \node[lat] (bot) at (0,0) {};
    \node[lat] (top) at (0,2) {};
    \node[lat] (a) at (-1,1) {};
    \node[lat] (b) at (0,1) {};
    \node[lat] (c) at (1,1) {};
    \draw[semithick] (bot) -- (a) -- (top) -- (b) -- (bot) -- (c) -- (top);
    \draw (top) node [right]{$1_R$};
    \draw (bot) node [right]{$0_R$};
    \draw (a) node [left]{$\etaR_1$};
    \draw (b) node [right]{$\etaR_2$};
    \draw (c) node [right]{$\etaR_3$};
    \draw (-2.5,1) node {$\Con(\bR) = $};
  \end{tikzpicture}
  \end{center}

  Each projection of $\bR$ onto 2 coordinates of $\bA^3$ is ``linked;'' 
  these binary projections are all readily seen to be
  $\{(0,0), (0,1), (1,0), (1,1)\} =  A \times A$ in this case. 
  Lemma~\ref{lem:abs2} tells us that this must be so.  For the set 
  $\{S \subseteq \nn  \mid \bigwedge_{i\in S} \etaR_i = 0_R\}$ that appears in the
  lemma is, in this example,
  $\sS = \{\{1,2\}, \{1,3\}, \{2,3\}, \{1,2,3\}\}$, and the projection of
  $R$ onto the coordinates in each minimal  set in $\sS$ must
  equal $A \times A$ by the lemma. 
  Now let $\bS \sdp \bA \times \bA \times \bA$ be the subdirect power of $\bA$
  with universe 
  \begin{align*}
    S &= \{(x,y,z)\in A^3\mid x+y+z=1 \text{ (mod $2$)}\}\\
    &= \{(1,0,0), (0,1,0), (0,0,1), (1,1,1) \}\\
    &= \{1, 2, 4, 7\}.
  \end{align*}
  All of the facts observed above about $\bR$ are also true of $\bS$.  In
  particular, $\Proj_{\{i,j\}} S = A_i \times A_j$ for 
  each pair $i\neq j$ in $\{1,2,3\}$.
  If both $\bR$ and $\bS$ belong to the set of relations 
  (or ``constraints'') of a single $\CSP(\sansS(\bA))$ instance, then the instance
  has no solution since $R \cap S = \emptyset$, despite the fact that  
  $\Proj_{\{i,j\}}\bR = \Proj_{\{i,j\}}\bS$  for each pair $i\neq j$ in
  $\{1,2,3\}$. To put it another way, $\bR$ and $\bS$  
  witness a failure of the \emph{2-intersection property}. The ``potato
  diagram'' of $\bA^3$ below depicts the elements of
  $R$ and $S$ as colored lines (with colors chosen to emphasize equality of
  the projections onto $\{1,2\}$). 

  \begin{center}
  \begin{figure}
    \caption{potatoes}
  \begin{tikzpicture}[scale=1]
    \draw (0,.5) ellipse (4mm and 12mm);
    \draw (2,.5) ellipse (4mm and 12mm);
    \draw (4,.5) ellipse (4mm and 12mm);
    \node[lat] (00) at (0,0) {};
    \node[lat] (01) at (0,1) {};
    \node[lat] (10) at (2,0) {};
    \node[lat] (11) at (2,1) {};
    \node[lat] (20) at (4,0) {};
    \node[lat] (21) at (4,1) {};
    \draw (00) node [below]{$0$};
    \draw (01) node [above]{$1$};
    \draw (10) node [below]{$0$};
    \draw (11) node [above]{$1$};
    \draw (20) node [below]{$0$};
    \draw (21) node [above]{$1$};
    \draw[red,thick] (00) -- (10) -- (20);
    \draw[blue,thick] (00) -- (11) -- (21);
    \draw[green,thick] (01) -- (10) -- (21);
    \draw[yellow,thick] (01) -- (11) -- (20);
    \draw (7,0.5) node {(lines are elements of $R$)};
  \end{tikzpicture}
  %% \nopagebreak[4]
  \vskip2mm
  \begin{tikzpicture}[scale=1]
    \draw (0,.5) ellipse (4mm and 12mm);
    \draw (2,.5) ellipse (4mm and 12mm);
    \draw (4,.5) ellipse (4mm and 12mm);
    \node[lat] (00) at (0,0) {};
    \node[lat] (01) at (0,1) {};
    \node[lat] (10) at (2,0) {};
    \node[lat] (11) at (2,1) {};
    \node[lat] (20) at (4,0) {};
    \node[lat] (21) at (4,1) {};
    \draw (00) node [below]{$0$};
    \draw (01) node [above]{$1$};
    \draw (10) node [below]{$0$};
    \draw (11) node [above]{$1$};
    \draw (20) node [below]{$0$};
    \draw (21) node [above]{$1$};
    \draw[yellow,thick] (01) -- (11) -- (21);
    \draw[red,thick] (00) -- (10) -- (21);
    \draw[green,thick] (01) -- (10) -- (20);
    \draw[blue,thick] (00) -- (11) -- (20);
    \draw (7,0.5) node {(lines are elements of $S$)};
  \end{tikzpicture}
  \end{figure}
  \end{center}

\end{example}

