

Suppose $\bA$ is a finite idempotent algebra with congruence relation $\theta \in \Con(\bA)$.
Let $\sI$ be an $n$-variable instance of $\CSP(\bA)$ and suppose
$\mathcal C = \{(\sigma_j, R_j) \mid 0\leq j < p\}$ is the (finite) set of constraints of $\sI$.
%% For $\bx = (x_0, x_1, \dots, x_{k-1}) \in A^k$, denote by $\bx/\theta$ the corresponding
%% tuple of equivalence classes of components of $\bx$.  That is,
%% \[
%% \bx/\theta = (x_0/\theta, x_1/\theta, \dots, x_{k-1}/\theta) \in (A/\theta)^k.
%% \]
Recall, %% for each constraint $(\sigma_j, R_j) \in \mathcal C$ the
%% quotient constraint $(\sigma_j, R_j/\theta)$ has 
%% $R_j/\theta := \{\br/\theta \in (A/\theta)^{\sigma_j} \mid \br \in R_j\}$,
%% and 
the quotient instance $\sI/\theta$ is the $n$-variable instance of
$\CSP(\bA/\theta)$ with constraint set given in (\ref{eq:100}) above.
We say that $\bA/\theta$ is a \emph{strongly tractable divisor} of $\bA$ if there exist
constants $c$ and $d$ and an algorithm that, given an instance $\sI$ of $\CSP(\bA)$,
\begin{itemize}
\item determines the full set $\mathcal S$ of solutions to the quotient instance $\sI/\theta$, and
\item takes at most $c |\sI|^d$ steps to complete.
\end{itemize}
Here $|\sI|$ denotes the size of the instance $\sI$, which we take to
be the length of a string encoding all the scopes and tuples of constraints
of $\sI$.
We use the adjective ``strongly'' in the definition above because the algorithm must determine
the full set of solutions to $\sI/\theta$, as opposed to just deciding whether or not
there is a solution.
On the other hand, the bound on the running time of the
algorithm is $c |\sI|^d$ and not $c|\sI/\theta|^d$, so this notion of tractability is maybe not
quite as strong as it first appears.


\subsection{Application: strongly tractable atop tractable}
Let $\bA$ be a finite idempotent algebra with a congruence $\theta$ whose
blocks belong to a tractable variety $\var{V}$, and 
suppose that $\bA/\theta$ is a strongly tractable divisor of $\bA$.
Our goal is to prove $\CSP(\bA)$ is tractable.

As a guide to intuition, we imagine applying the algorithm described in this section to situations
in which the number of congruence classes of $\theta$---and therefore the number of
solutions to quotient instances---is relatively small.  To this end, we
call $\theta\in \Con(\bA)$ a \emph{coarse congruence}
if there are constants $c$ and $d$ such that, for every instance $\sI$ of $\CSP(\bA)$,
the number of solutions to the quotient instance $\sI/\theta$ is bounded above by $c|\sI|^d$.


Recall Fact~\ref{fact:soln-quotient} above which says that if the quotient instance
$\sI/\theta$ has no solution, then $\sI$ has no solution.
This and the notion of a ``coarse congruence'' suggest the following algorithm for solving $\CSP(\bA)$.
Given an instance $\sI$ of $\CSP(\bA)$,
\begin{enumerate}
\item compute the solution set $\mathcal S$ of the quotient instance $\sI/\theta$ of $\CSP(\bA/\theta)$;
\item for each solution $\bx/\theta\in \mathcal S$ and corresponding block instance $\sI_{\bx}$,
check %% (in polynomial-time)
whether there is a solution to $\sI_{\bx}$.
%% If there is a solution, then this will also be a solution to the original instance.
%% Since $\bB_i \in \var{V}$, a tractable variety, we can determine in polynomial time whether the restricted instance
(If $\sI_{\bx}$ has a solution, then so does $\sI$---in fact, a solution to $\sI_{\bx}$ \emph{is} a solution to $\sI$.)
\end{enumerate}
If for all $\bx/\theta \in S$ the block restricted instance $\sI_{\bx}$ has no solution, then $\sI$ has no solution.

The number of steps in stage (1) of the algorithm is bounded by a polynomial in $|\sI|$
since we assumed $\bA/\theta$ is a strongly tractable divisor of $\bA$.
In stage (2), for each solution $\bx/\theta \in \mathcal S$,
determining whether there is a solution to $\sI_{\bx}$ has polynomial time complexity since
$\CSP(\bB_{\chi(0)}, \bB_{\chi(1)}, \dots, \bB_{\chi(n-1)})$ is tractable.  Therefore,
if we can find some constants $c$ and $d$ such that the number $|\mathcal S|$ of solutions to the quotient instance
$\sI/\theta$ is bounded above by $c|\sI|^{d}$, then stage (2) can be completed in
polynomial 
  \ifthenelse{\boolean{footnotes}}{%
    time.\footnote{To be a little 
      more precise, the time complexity of stage (2) of the algorithm is bounded
      by $c|\sI|^{d} \cdot c' |\sI|^{d'} \approx C|\sI|^{D}$, where $c'$ and $d'$ are found
      by bounding the complexity of 
      $\CSP(\bB_{\chi(0)}, \bB_{\chi(1)}, \dots, \bB_{\chi(n-1)})$ for a
      ``worst-case product'' of $\theta$ classes.}
  }{time.}
The next proposition summarizes what we have shown.
\begin{prop}
  \label{prop:chain-atop-tractable}
  Suppose $\bA$ is a finite idempotent algebra with $\theta \in \Con(\bA)$ satisfying the following conditions:
  \begin{itemize}
  \item  $\bA/\theta$ is strongly tractable;
  \item  $\theta$ blocks belong to a tractable variety;
  \item  $\theta$ is coarse.
  \end{itemize}
  Then $\CSP(\bA)$ is tractable.
\end{prop}
