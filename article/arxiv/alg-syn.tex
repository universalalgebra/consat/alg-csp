
We now present a new result (Theorem~\ref{thm:fry-pan2})
that, like the Rectangularity Theorem, aims to describe some of the elements that
must belong to certain subdirect products.
The conclusions we draw are weaker than those of
Theorem~\ref{thm:rectangularity}.
However, the hypotheses required here are simpler, and the motivation
is different.

We have in mind subdirect products of ``heterogeneous'' 
families of algebras. What we mean by this is 
described and motivated as follows:
let $\sC_1$, $\dots$, $\sC_m$
be classes of algebras, all of the same signature. Perhaps we are lucky enough
to possess a single algorithm that
proves the algebras in $\bigcup\sC_i$ are jointly
tractable (defined in Section~\ref{sec:inst-size-tract}).
Suppose instead that we are a little less fortunate
and, although we have no such single
algorithm at hand, at least we do happen to know that
the classes $\sC_i$ are \emph{separately tractable}.
By this we mean that for each $i$ we have a 
proof (or algorithm) $\P_i$ witnessing the tractability of
$\CSP(\sC_i)$.
It is natural to ask whether and under what conditions it might be possible to
derive from $\{\P_i \mid 1\leq i \leq m\}$ a single proof (algorithm)
establishing that the algebras in 
$\bigcup \sC_i$ are \emph{jointly tractable},
so that $\CSP(\bigcup \sC_i)$ is tractable.

The results in this section take a small
step in this direction by considering two special
classes of algebras that are known to be separately tractable,
and demonstrating that they are, in fact, jointly tractable.
We apply this tool in Section~\ref{sec:block-inst} where
we prove tractability of a \csp involving algebras that were
already known to be tractable, but not previously known to be jointly tractable.


The results here involve special term operations called
cube terms and transitive terms.
A $k$-ary idempotent term $t$ is a \emph{cube term}
if for every
coordinate $i \leq k$, $t$ satisfies an identity of the form
$t(x_1, \dots, x_k) \approx y$, where
$x_1,\dots, x_k \in \{x, y\}$ and $x_i = x$.
%% (See \cite{MR2563736} and \cite{MR2900858}.)
A $k$-ary operation $f$ on a set $A$ is called
\emph{transitive in the $i$-th coordinate} if for every $u, v \in A$,
there exist $a_1,\dots, a_k \in A$ such that $a_i = u$
and $f(a_1 ,\dots, a_n) = v$. Operations
that are transitive in every coordinate are called \emph{transitive}. 


\begin{fact}
  \label{fact:cubes-are-trans}
  Let $\bA$ be a finite idempotent algebra and suppose $t$ is a cube term operation on $\bA$.
  Then $t$ is a transitive term operation on $\bA$.
\end{fact}
\begin{proof}
  Assume $t$ is a $k$-ary cube term operation on $\bA$ and fix $0\leq i<k$.  We will prove that $t$
  is transitive in its $i$-th argument.  Fix $u, v \in A$.
  We want to show there exist $a_0,\dots, a_{k-1} \in A$ such that $a_{i-1} = u$
  and $t(a_0 ,\dots, a_{k-1}) = v$. Since $t$ is a cube term, it satisfies an identity of the form
    $t(x_1, \dots, x_k) \approx y$,
  where $(\forall j)(x_j \in \{x, y\})$ and $x_i = x$.
  So we simply substitute $u$ for $x$ and $v$ for $y$ in 
  the argument list in of this identity. %Equation~(\ref{eq:2}).
  Denoting this substitution by $t[u/x, v/y]$, we have
  $t[u/x, v/y]= v$, proving that $t$ is transitive.
\end{proof}

\begin{fact}
  \label{fact:pseudovar}
  The class
  \begin{equation}
    \label{eq:0001}
    \sT = \{\bA \mid \bA \text{ finite and every subalgebra of $\bA$ has a transitive term op}\}
  \end{equation}
  is closed under the taking of homomorphic images, % ($\sansH$)
  subalgebras,  % ($\sansS$), 
  and finite products. % ($\sansP_{\mathrm{fin}}$).
  That is, $\sT$ is a \emph{pseudovariety}.
\end{fact}


We also require the following obvious fact about
nontrivial algebras in a Taylor variety. (We call an algebra \emph{nontrivial}
if it has more than one element.)
\begin{fact}
  \label{fact:nonconstant-terms-exist}
  If $\bA$ and $\bB$ are nontrivial algebras in a Taylor variety $\var{V}$, then for
  some $k>1$ there is a $k$-ary term $t$ in $\var{V}$ such that $t^{\bA}$ and
  $t^{\bB}$ each depends on at least two of its arguments.
\end{fact}

Finally, we are ready for the main result of this section.

\begin{theorem}
\label{thm:fry-pan2}
  Let $\bA_0, \bA_1, \dots, \bA_{n-1}$ be finite idempotent algebras in a Taylor
  variety and assume there exists a proper nonempty subset $\alpha \subset \nn$ such that
  \begin{itemize}
  \item $\bA_i$ has a cube term for all $i \in \alpha$,
  \item $\bA_j$ has a sink $s_j \in A_j$ for all $j \in \alpha'$;
    let $\bs \in \prod_{\alpha'}A_j$ be a tuple of sinks.
  \end{itemize}
   If $\bR \sdp \prod_{\nn} \bA_i$, then the set $X:=R_\alpha \times \{\bs\}$
  is a subuniverse of $\bR$.
\end{theorem}
\begin{remark}
To foreshadow applications of
Theorem~\ref{thm:fry-pan2}, imagine we have 
algebras of the type described, and a collection
$\sR$ of subdirect products of these algebras. Suppose also that we have somehow
determined that the intersection of the $\alpha$-projections of these subdirect
products is nonempty, say,
\[\bx_\alpha \in \bigcap_{R \in \sR} R_\alpha.\]
Then the full intersection $\bigcap \sR$ will also be nonempty, since
according to the theorem it must
contain the tuple that is $\bx_\alpha$ on $\alpha$ and $\bs$ off $\alpha$.
\end{remark}

\begin{proof}
  Fix $\bx \in X$, so $\bx\circ \alpha' = \bs$ and $\bx \circ \alpha \in R_\alpha$. 
  We will prove that $\bx \in R$.
  Define $\bA_{\alpha} =\prod_{\alpha}\bA_i$ and $\bA_{\alpha'} =\prod_{\alpha'}\bA_i$, so 
  $R_{\alpha}$ is a subuniverse of $\bA_\alpha$ and $R$ is a subuniverse of
  $\bA_\alpha \times \bA_{\alpha'}$.

  By Fact~\ref{fact:cubes-are-trans}, 
  for each $i \in \alpha$ every subalgebra of $\bA_i$
  has a transitive term operation.  Moreover, the class $\sT$ defined in~(\ref{eq:0001}) 
  is a pseudovariety, so every subalgebra of 
  $\bA_{\alpha}$ has a transitive term operation.  Suppose there are $J$ subalgebras 
  of $\bA_{\alpha}$ and let $\{t_j\mid 0\leq j < J\}$ denote the collection
  of transitive term operations, one for each subalgebra. % of $\bA_\alpha$.
  Then it is not hard to prove that 
  $t := t_0 \star  t_1 \star \cdots \star  t_{J-1}$
  is a transitive term for every subalgebra of $\bA_\alpha$.
  (Recall, $\star$ was defined at the end of Section~\ref{sec:absorption-thm}.)  
  In particular $t$ is transitive for the subalgebra with universe $R_{\alpha}$. 
  %% Fix $N$ and a
  Assume $t$ is $N$-ary.
  
  Fix $j\in \alpha'$.  As usual, $t^{\bA_j}$ denotes the interpretation of 
  $t$ in $\bA_j$. We may assume without loss of generality that $t^{\bA_j}$ 
  depends on its first argument, at position 0. (It must depend on at least one of
  its arguments by idempotence.) 
  Now, since $\bR$ is a subdirect product of $\prod_{\nn}\bA_i$, 
  there exists $\br^{(j)} \in R$ such that $\br^{(j)}(j) = s_j$, the sink in $\bA_j$.  
  Since   $\bx\circ \alpha\in R_\alpha$ and since $t^{\bA_\alpha}$
  is transitive over $\bR_\alpha$, there exist
  $\br_1, \dots, \br_{N-1}$ in $R$ such that 
  \begin{align*}
   \vy^{(j)}&:= t^{\bA_{\alpha} \times \bA_{\alpha'}}(\br^{(j)}, \br_1, \dots, \br_{N-1})\\
      &= 
    (t^{\bA_{\alpha}}(\br^{(j)}\circ \alpha, \br_1\circ \alpha, \dots, \br_{N-1}\circ \alpha),
    t^{\bA_{\alpha'}}(\br^{(j)}\circ \alpha', \br_1\circ \alpha', \dots, \br_{N-1}\circ \alpha'))
  \end{align*}
  belongs to $R$  and satisfies
  $\vy^{(j)}\circ \alpha = \bx \circ \alpha$ and
  $\vy^{(j)}(j) = s_j$.

  (To make the role played by transitivity here
  more transparent, note that we asserted the existence of elements in $R$
  whose ``$\alpha$-segments,'' $\br_1\circ \alpha, \dots, \br_{N-1}\circ \alpha$
  could be plugged in for all but one of the arguments of $t^{\bA_\alpha}$, 
  resulting in a map (unary polynomial) taking $\br^{(j)}\circ \alpha$ to
  $\bx\circ \alpha$. It is the transitivity of $t^{\bA_\alpha}$ over $\bR_\alpha$
  that justifies this assertion.)

  If $\alpha' = \{j\}$, we are done, since $\vy^{(j)} = \bx$ in that case.
  If $|\alpha'|>1$, then we repeat the foregoing procedure for each 
  $j \in \alpha'$ and obtain a subset $\{\vy^{(j)} \mid j \in \alpha'\}$ of $R$,
  each member of which agrees with $\bx$ on $\alpha$ and has a sink in some position
  $j\in \alpha'$. 

  Next, choose distinct $j, k \in \alpha'$. Suppose $w$ is a Taylor term for $\var{V}$.
  Then by Fact~\ref{fact:nonconstant-terms-exist} we may assume without loss of
  generality that $w^{\bA_j}$ depends on its $p$-th argument and $w^{\bA_k}$ depends
  on its $q$-th argument, for some $p\neq q$. Consider
  \begin{align*}
    \bz:= w^{\Pi \bA_i}(\vy^{(j)}, %% \vy^{(j)}, 
    \dots, \vy^{(j)}, & \, \vy^{(k)}, \vy^{(j)}, \dots, \vy^{(j)})\\
    %&\uparrow\\
    &\; ^{\widehat{\lfloor}} \, \text{$q$-th argument}
  \end{align*}
  Evidently, $\bz(j) = s_j$, $\bz(k) = s_k$, and 
  $\bz \circ \alpha = \bx \circ \alpha$ by idempotence, since, when restricted to
  indices in $\alpha$, all the input arguments agree and are equal to $\bx \circ \alpha$. 
  If $\alpha' = \{j, k\}$, we are done.  Otherwise, choose
  $\ell \in \alpha'-\{j, k\}$, and again $w^{\bA_\ell}$ depends on at
  least one of its 
  \ifthenelse{\boolean{footnotes}}{%
    arguments,\footnote{It
      clearly doesn't matter on which argument $w^{\bA_\ell}$ depends.}
  }{arguments,}
  say, the $u$-th.  Let
  \begin{align*}
    \bz':= w^{\Pi \bA_i}(\bz, %% \vy^{(j)}, 
    \dots, \bz, & \, \vy^{(\ell)}, \bz \dots, \bz).\\
    %&\uparrow\\
    &\; ^{\widehat{\lfloor}} \, \text{$u$-th argument}
  \end{align*}
  Then $\bz'$ belongs to $R$, agrees with $\bx$ on $\alpha$, and satisfies
  $\bz'(j) = s_j$, $\bz'(k) = s_k$, and   $\bz'(\ell) = s_\ell$. 
  Continuing in this way until the set $\alpha'$ is exhausted produces
  an element in $R$ that agrees with $\bx$ everywhere. % $\alpha \cup \alpha'$.
  In other words,  $\bx$ itself belongs to $R$.
\end{proof}

In Section~\ref{sec:block-inst} we apply
Theorem~\ref{thm:fry-pan2} 
in the special case where
``has a cube term''  in the first hypothesis 
is replaced with ``is abelian.'' Let us be explicit.

\begin{corollary}
\label{cor:fry-pan}
  Let $\bA_1, \dots, \bA_{n-1}$ be finite idempotent algebras in a
  locally finite Taylor variety. 
  Suppose there exists $0< k < n-1$ such that 
  \begin{itemize}
  \item $\bA_i$ is abelian for all $i < k$;
  \item $\bA_i$ has a sink $s_i \in A_i$ for all $i \geq k$.
  \end{itemize}
  If $\bR \sdp \prod \bA_i$, then
  $R_{\kk} \times \{s_k\} \times \{s_{k+1}\} \times \cdots \times \{s_{n-1}\}  \subseteq R$.
\end{corollary}
\begin{proof}
  Since $\bA_{\kk} := \prod_{i<k} \bA_i$ is abelian and lives in a locally finite
  Taylor variety, there exists a term $m$ such that $m^{\bA_{\kk}}$ is a \malcev
  operation on $\bA_{\kk}$ (Theorem~\ref{thm:type2cp}).
  Since a \malcev term is a cube term, the result follows from Theorem~\ref{thm:fry-pan2}.
\end{proof}

\ifthenelse{\boolean{arxiv}}{%
  As an alternative, a direct proof of Corollary~\ref{cor:fry-pan} appears in 
  Section~\ref{sec:proof-fry-pan-cor} below.
}{As an alternative, a direct proof of Corollary~\ref{cor:fry-pan} appears in
  the extended version of the present paper,~\cite{Bergman-DeMeo}.}

