
In this section we survey some of the theory related to 
an important concept called \emph{absorption}, which was invented
by Libor Barto and Marcin Kozik. After introducing the concept, we discuss some of the properties that make it
so useful. 
The main results in this section are not new. The only possibly novel contributions are some
straightforward observations in Section~\ref{sec:linking-lemmas} (which have likely been
observed by others). Our intention here is merely to collect and present these
known results in a way that makes them easy to apply in later sections and future work.

Let $\bA = \<A, F^{\bA}\>$ be a finite algebra in the Taylor variety $\var{V}$ 
and let $t\in \sansClo(\bA)$ be a $k$-ary term operation.
A subalgebra  $\bB = \<B, F^{\bB}\> \leq \bA$ is said to be 
\defn{absorbing in $\bA$ with respect to $t$} if
for all $1\leq j\leq k$ and for all 
$(b_1, \dots, b_{j-1}, a, b_{j+1}, \dots, b_k)\in B^{j-1}\times A \times B^{k-j}$
we have
\[
t^{\bA}(b_1, \dots, b_{j-1}, a, b_{j+1}, \dots, b_k)\in B.
\]
In other terms,
$t^{\bA}[B^{j-1}\times A \times B^{k-j}] \subseteq B$,
for all $1\leq j \leq k$.
Here, $t^\bA[D]$ denotes the set $\{ t^\bA(x) \mid x\in D \}$.

The notation $\bB \absorbing \bA$ means that
$\bB$ is an absorbing subalgebra of $\bA$ with respect to some term.
If we wish to be explicit about the term, we write $\bB \absorbing_t \bA$.
We may also call $t$ an ``absorbing term'' for $\bB$ in this case.
The notation $\bB \minabsorbing \bA$ 
means that $\bB$ is a minimal absorbing subalgebra of $\bA$, that is, 
$B$ is minimal (with respect to set inclusion) among the absorbing subuniverses
of $\bA$. 
An algebra is said to be \defn{absorption-free} if it has no proper absorbing
subalgebras.

\subsection{Absorption theorem}
\label{sec:absorption-thm}
In later sections, we will make frequent use of a 
powerful tool of Libor Barto and Marcin Kozik called the 
``Absorption Theorem'' (Thm~2.3 of~\cite{MR2893395}).
This result concerns the special class of ``linked'' subdirect products.  
A subdirect product $\bR \sdp \bA_0 \times \bA_1$
is said to be \defn{linked} if it satisfies the following:
for $a, a' \in \Proj_0 R$ there 
exist elements $c_0, c_2, \dots, c_{2n} \in A_0$ and
$c_1, c_3, \dots, c_{2n+1} \in A_1$ such that $c_0 = a$, 
$c_{2n} = a'$, and for all $0\leq i<n$, 
$(c_{2i},c_{2i+1})\in R$ and $(c_{2i+2},c_{2i+1})\in R$.
Here is an easily proved fact that provides some equivalent ways to define ``linked.''
\begin{fact}
Let $\bR \sdp \bA_0 \times \bA_1$, let $\etaR_i = \ker(\bR \onto \bA_i)$
denote the kernel of the projection of $\bR$ onto its $i$-th coordinate, and let 
$R^{-1} = \{(y,x) \in A_1 \times A_0 \mid (x,y) \in R\}$. 
Then the following are equivalent:
\begin{enumerate}
\item $\bR$ is linked;
\item $\etaR_0\join \etaR_1 = 1_R$;
\item if $a, a' \in \Proj_0 R$, then $(a,a')$ is in the transitive closure of $R\circ R^{-1}$.
\end{enumerate}
\end{fact}


\begin{theorem}[Absorption Theorem \protect{\cite[Thm~2.3]{MR2893395}}]
\label{thm:absorption}
If $\var{V}$ is an idempotent locally finite variety, then the following are equivalent:
\begin{itemize}
\item $\var{V}$ is a Taylor variety;
\item if $\bA_0, \bA_1 \in \var{V}$ are finite absorption-free algebras, 
  and if $\bR \sdp \bA_0 \times \bA_1$ is linked, then $\bR = \bA_0 \times \bA_1$.
\end{itemize}
\end{theorem}

Before moving on to the next subsection, we need one more definition.  
If $f\colon A^\ell\to A$ and 
$g\colon A^m\to A$ are 
$\ell$-ary and $m$-ary operations on $A$,
then by $f \star g$ we mean the $\ell m$-ary operation 
that maps each
$\ba = (a_{1 1},\dots, a_{1 m}, a_{2 1}, \dots,  a_{2 m},\dots, a_{\ell m}) \in A^{\ell m}$
to
\[
(f\star g)(\ba)
= f(g(a_{1 1}, \dots, a_{1 m}), g(a_{2 1}, \dots, a_{2 m}), \dots,  g(a_{\ell 1}, \dots, a_{\ell m})).
\]
%% \ifthenelse{\boolean{extralong}}{%
%%   % wjd 2016.11.08: omitting from arxiv and journal version
  %% \[
  %% \ba = (a_{1 1},\dots, a_{1 m}, a_{2 1}, \dots,  a_{2 m},\dots, a_{\ell m}) \in A^{\ell m}
  %% \]
  %% to $(f\star g) \,\ba
  %% = 
  %% f(g(a_{1 1}, \dots, a_{1 m}), g(a_{2 1}, \dots, a_{2 m}), \dots,  g(a_{\ell 1}, \dots, a_{\ell m}))$.
%% }{%
%%   $(a_{1 1},\dots, a_{1 m}, a_{2 1}, \dots,  a_{2 m},\dots, a_{\ell m}) \in A^{\ell m}$ to
%%   $f(g(a_{1 1}, \dots, a_{1 m}), g(a_{2 1}, \dots, a_{2 m}), \dots,  g(a_{\ell 1}, \dots, a_{\ell m}))$.
%% }



\subsection{Properties of absorption}
%% \subsection{General Facts}
In this section we prove some easy facts about algebras that hold
quite generally.  In particular, we don't assume the presence of Taylor terms or
other \malcev conditions in this section.  However, for the sake of
simplicity, we do assume that all algebras are finite and belong to a single
idempotent variety, though some of the results of this section hold for
infinite algebras as well.  
Almost all of the results in this section are well known.
The only exceptions are mild generalizations of facts that have already appeared in print.
We have restated or reworked some of the known results in a way that makes them easier
for us to apply. 

\newcommand\sseq{\ensuremath{\subseteq}}
\begin{lemma}
\label{lem:fact1}
Let $\bA$ be an idempotent algebra with terms $f$ and $g$. If
$\bB\absorbing \bA$ with respect to either $f$ or $g$, then
$\bB\absorbing \bA$ with respect to $f\star g$. 
\end{lemma}
\begin{proof}
In this proof, to keep the notation simple,
let $ABB\cdots B$ denote the usual Cartesian product 
$A\times B \times B \times \cdots \times B$, and
let $t=f\star g$.
We only handle the $j=1$ 
case of the definition in Section~\ref{sec:absorption}. 
(The general case is equally simple, but the notation becomes tedious.)
That is, we prove the following subset inclusion:
\[
t[ABB\cdots B]
= f\bigl[g[ABB\cdots B] \times 
g[BB\cdots B] \times \cdots
\]
\[\phantom{XXXXXXXXXXXXXX}\cdots \times
g[BB \cdots B]\bigr]\sseq B. 
\]
Suppose first that $f$ is the absorbing term. Since $B$ is a subalgebra,
$g[BB\cdots B]$ is contained in $B$. (This holds for any term.) Hence, 
$t[ABB\cdots B]$ is contained in $f[ABB\cdots B] \sseq B$. 
On the other hand, if $g$ is the absorbing term, then
$t[ABB \cdots B] \sseq f[BB \cdots B] \sseq B$, again because $B$ is a
subalgebra. 
\end{proof}
\begin{corollary}
  \label{cor:fact1gen}
  Fix $p <\omega$ and suppose $\{t_0, t_1, \dots, t_{p-1}\}$ is a set of terms in the variety $\var{V}$.
  If $\bA$ and $\bB$ are finite algebras in $\var{V}$ and if $\bB\absorbing_{t} \bA$ for some
  $t$ in $\{t_0, t_1, \dots, t_{p-1}\}$, then $\bB \absorbing_s \bA$, where 
  $s = t_0 \star t_1 \star \cdots \star t_{p-1}$.
\end{corollary}
\begin{proof}
  By induction on~$p$.
  \end{proof}

\begin{lemma}[\protect{\cite[Prop~2.4]{MR2893395}}] 
\label{lem:bk-prop-2-4}
  Let $\bA$ be an algebra.
  \begin{itemize}
  \item If $\bC \absorbing \bB \absorbing \bA$, then $\bC \absorbing \bA$.
  \item If $\bB \absorbing_f \bA$ and $\bC \absorbing_g \bA$ and $B \cap C\neq \emptyset$, then 
    $B \cap C$ is an absorbing subuniverse of $\bA$ with respect to $t = f\star g$.
  \end{itemize}
\end{lemma}

\begin{corollary}
\label{cor:fact2}
For $i=0, 1$, let $\bA_i$ and $\bB_i$ be algebras in the variety $\var{V}$ and suppose
$\bB_0 \absorbing_f \bA_0$ and $\bB_1 \absorbing_g \bA_1$.
Then $\bB_0\times \bB_1$ is absorbing in $\bA_0\times \bA_1$ with respect to $f\star g$.
\end{corollary}
\begin{proof}
Since $\bB_0 \absorbing_f \bA_0$, it follows that
$\bB_0\times \bA_1 \absorbing \bA_0\times \bA_1$  with respect to the same term $f$.
Similarly $\bA_0 \times \bB_1 \absorbing_g \bA_0\times \bA_1$.
Hence, $\bB_0\times \bB_1=(\bB_0\times \bA_1) \cap (\bA_0\times \bB_1)$ is 
absorbing in $\bA_0\times \bA_1$ with respect to 
$f\star g$, by Lemma~\ref{lem:bk-prop-2-4}.
\end{proof}
Corollary \ref{cor:fact2} generalizes to finite products and \emph{minimal} 
absorbing subalgebras. We record these observations next. 
(A proof appears in~\ref{sec:proof-cor-min-abs-prod}.)
\begin{lemma}
\label{lem:min-abs-prod}
Let $\bB_i \leq \bA_i$ $(0\leq i < n)$ be algebras in the variety $\var{V}$, 
let $\bB := \bB_0\times \bB_1\times \cdots \times \bB_{n-1}$, and let
$\bA := \bA_0\times \bA_1\times \cdots \times \bA_{n-1}$.
If $\bB_i\absorbing_{t_i}\bA_i$ (resp., $\bB_i\minabsorbing_{t_i}\bA_i$) for each $0\leq i < n$,
then $\bB \absorbing_s \bA$ (resp., $\bB \minabsorbing_s \bA$) 
where $s:= t_0\star t_1 \star \cdots \star t_{n-1}$.
\end{lemma}
An obvious but important consequence of Lemma~\ref{lem:min-abs-prod} is that a
(finite) product of finite idempotent algebras is absorption-free if
each of its factors is absorption-free.

We conclude this section with a few more properties of absorption that will 
be useful below.
The first is a simple observation with a very easy proof.
\begin{lemma}
\label{lem:restriction}
Let $\bB$ and $\bC$ be subalgebras of a finite idempotent algebra $\bA$.
Suppose $\bB \absorbing_t \bA$, and suppose $D = B\cap C \neq \emptyset$.
Then the restriction of $t$ to $C$ is an absorbing term for $D$ in $C$, whence 
$D\absorbing C$.
\end{lemma}
Despite its simplicity, Lemma~\ref{lem:restriction} is quite useful for
proving that certain algebras are absorption-free.
%% (See Section~\ref{sec:example-absorpt-free} for examples.)
%% >>>> wjd: can't find thie reference sec:example-absorpt-free.  It must have been
%% >>>>      deleted during chb's revisions.  If we find it, we could uncomment
%% >>>>      the reference in parentheses in the line above.
Along with Lemma~\ref{lem:min-abs-prod}, Lemma~\ref{lem:restriction} 
yields the following result (to be applied in Section~\ref{sec:tayl-vari-rect}):
\begin{lemma}
\label{lem:gen-abs1}
Let
$\bA_0$, $\bA_1$, $\dots$, $\bA_{n-1}$
be finite algebras with $\bB_i\absorbing \bA_i$ for each $i$.
Suppose $\bR \leq \myprod_{i}\bA_i$ and $R \cap \myprod_i B_i \neq \emptyset$.
Then $R \cap \myprod_i B_i$ is an absorbing subuniverse of $\bR$.
\end{lemma}
\begin{proof}
  Let $\bA = \myprod_i \bA_i$ and $\bB = \myprod_i \bB_i$.
  By Lemma~\ref{lem:min-abs-prod}, $\bB \absorbing_t \bA$, so the result
  follows Lemma~\ref{lem:restriction} if we put $C = R$.
\end{proof}


For $0<j\leq k$, we say that a $k$-ary term operation $t$ of an algebra $\bA$
\emph{depends on its $j$-th argument} provided there exist $a_0, a_1, \dots, a_{k-1}$
such that $p(x) := t(a_0, \dots, a_{j-2}, x, a_{j}, \dots, a_{k-1})$ is a
nonconstant polynomial of $\bA$.
Let \bA\ be an algebra, and $s \in C \subseteq A$. We call $s$ a
\emph{sink for} $C$ provided that for any term 
$t \in \sansClo_k(\bA)$ and for any $0< j \leq k$, if 
$t$ depends on its $j$-th argument, then 
$t(c_0, c_1, \dots, c_{j-2}, s, c_{j}, \dots, c_{k-1})=s$
for all $c_i \in C$.  If $C$ is a subuniverse, then $\{s\}$ is absorbing, in a very strong way. In fact,
it is easy to see that if an absorbing subuniverse $B$ intersects
nontrivially with a set containing a sink, then $B$ must also contain the sink.
We record this as
\begin{lemma}
\label{lem:sink}
  If $B \absorbing A$, if $s$ is a sink for $C\subseteq A$, and 
  if $B\cap C \neq \emptyset$, then $s\in B$.
\end{lemma}

Proof of the next lemma
appears in~\ref{sec:proof-lemma-sdp-general} below.
\begin{lemma}
\label{lem:sdp-general}
Let
$\bA_0$, $\bA_1$, $\dots$, $\bA_{n-1}$
be finite idempotent algebras of the same type, and
  suppose $\bB_i \minabsorbing \bA_i$ for $0\leq i< n$.
  Let $\bR \sdp \bA_0 \times \bA_1 \times \cdots \times \bA_{n-1}$, 
  and let $R' = R \cap (B_0 \times B_1 \times \cdots \times B_{n-1})$.  
  If $R'\neq \emptyset$, then $\bR' \sdp \bB_0 \times \bB_1 \times \cdots \times \bB_{n-1}$.
\end{lemma}

For a proof of the next result, see~\cite[Prop~2.15]{MR2893395}.

\begin{lemma}[\protect{\cite[cf.~Prop~2.15]{MR2893395}}]
\label{lem:linked-absorber}
Let $\bA_0$ and $\bA_1$ be finite idempotent algebras of the same type, let
$\bR \sdp \bA_0 \times \bA_1$ and assume $\bR$ is linked.
If $\bS \absorbing \bR$, then $\bS$ is also linked.
\end{lemma}

Finally, we come to a very useful fact about absorption that we exploit heavily
in the sections that follow. 
\ifthenelse{\boolean{arxiv}}{%
  (This is proved in~\cite[Lem~4.1]{MR3374664}, and in
  Section~\ref{sec:proof-that-abelian} below.)
}{(This is proved in~\cite[Lem~4.1]{MR3374664}, and in
  the extended version of the present paper~\cite{Bergman-DeMeo}.)
}
\begin{lemma}[\protect{\cite[Lem~4.1]{MR3374664}}]
\label{lem:abelian-AF}
  Finite idempotent abelian algebras are absorption-free. 
\end{lemma}


 \subsection{Linking is easy, sometimes}
  \label{sec:linking-lemmas}
  We will apply the Absorption Theorem frequently below, so we pause here to
  consider one of the hypotheses of the theorem that might seem less familiar to
  some of our readers.  Specifically, one might wonder when
   we can expect a subdirect product to be \emph{linked}, as is required 
  of $\bR \sdp \bA_0\times \bA_1$ in the statement of the Absorption Theorem.
  Here we consider a few special cases in which this hypothesis comes
  essentially for free.  Most of the proofs in this section, as well as some
  in later sections, depend on the following elementary observation about subdirect
  products:
  \begin{lemma}
    \label{lem:basic}
    Let $\bA_0$ and $\bA_1$ be algebras.  Suppose
    $\bR \sdp \bA_0 \times \bA_1$ and let $\etaR_i = \ker(\bR \onto \bA_i)$
    denote the kernel of the $i$-th projection of $\bR$ onto $\bA_i$. 
    \begin{enumerate}
    \item 
      If $\bA_0$ is simple, then either $\etaR_0 \join \etaR_1 = 1_R$ or $\etaR_0 \geq \etaR_1$.
    \item If $\bA_0$ and $\bA_1$ are both simple, then either $\etaR_0 \join \etaR_1 = 1_R$
      or $\etaR_0 = 0_R = \etaR_1$.
    \end{enumerate}
  \end{lemma}
  \begin{proof}
    The congruence relation $\etaR_0 \in \Con \bR$ is maximal, since 
    $\bR/\etaR_0 \cong \bA_0$ and $\bA_0$ is simple. 
    Therefore, if $\etaR_0 \join \etaR_1 < 1_R$, then $\etaR_0 =\etaR_0 \join \etaR_1$, so 
    $\etaR_1 \leq \etaR_0$, proving (1). If $\bA_1$ is also simple, then the
    same argument, with the roles of $\etaR_0$ and $\etaR_1$ reversed, shows that
    $\etaR_0 \leq \etaR_1$, so   
    $\etaR_0 = \etaR_1$.  Since $\etaR_0 \meet  \etaR_1 = 0_R$, we see that both 
    projection kernels must be $0_R$ in this case.
  \end{proof}
  An immediate corollary is that, in case both factors in the product
  $\bA_0 \times \bA_1$ are simple,
  the linking required in the Absorption Theorem holds under weaker hypotheses. 
  \begin{corollary}
    \label{cor:rect-two_factors-pre}
    Let $\bA_0$ and $\bA_1$ be simple algebras and suppose $\bR \sdp \bA_0 \times \bA_1$.
    If $\etaR_0 \neq \etaR_1$ or $\bA_0 \ncong \bA_1$, then $\bR$ is linked.
  \end{corollary}

  Another corollary of~\ref{lem:basic}
  holds in a locally finite Taylor variety.
  When one factor of $\bA_0 \times \bA_1$ is
  abelian and the other is simple and nonabelian, then we get linking for
  free. That is, in a locally finite Taylor variety,
  \emph{every subdirect product of $\bA_0 \times \bA_1$ is linked}, as we now prove. 
  \begin{corollary}
    \label{cor:S-NA-AF_A-pre}
    Suppose $\bA_0$ and $\bA_1$ are algebras in a locally finite Taylor variety.
    If $\bA_0$ is abelian and $\bA_1$ is simple and nonabelian, then
    every subdirect product of $\bA_0 \times \bA_1$ is linked.     Moreover, if 
    $\bR \sdp \bA_0 \times \bA_1$ and if $\bB_1 \minabsorbing \bA_1$,
    then  $\bR$ intersects $\bA_0\times \bB_1$ nontrivially and this intersection
    forms a linked subdirect product of $\bA_0 \times \bB_1$.
  \end{corollary}
  \begin{proof}
    Suppose  $\bR \sdp \bA_0 \times \bA_1$ is not linked. Since $\bA_1$ is simple
    we have $\etaR_0 \leq \etaR_1$ by Lemma~\ref{lem:basic}.
    Therefore, $\etaR_0 = \etaR_0 \meet \etaR_1 = 0_R$.  But
    then $\bR \cong \bR/\etaR_0 \cong \bA_0$ is abelian, while
    $\bR/\etaR_1 \cong \bA_1$ is nonabelian. This is a contradiction
    since, by Remark~\ref{rem:abelian-quotients},
    $\bR/\theta$ is abelian for all
    $\theta \in \Con \bR$.  Therefore, $\bR$ is linked.
    For the second part, since $\bR$ is a subdirect product, it follows that
    $R \cap (A_0\times B_1)$ is nonempty and, by
    Lemma~\ref{lem:gen-abs1},  $\bR \cap (\bB_0\times \bA_1)$ is
    absorbing in $\bR$. Therefore, by Lemma~\ref{lem:linked-absorber},
    the intersection must also be linked. 
  \end{proof}
  We can extend the previous result to multiple abelian factors by collecting
  them into a single factor.
  We use the notation $\nn := \{0,1,\dots, n-1\}$ and $k':=\nn-\{k\}$.
  For example,
  \[0' :=  \{1,2 ,\dots, n-1\} \quad \text{ and }\quad \bR_{0'} := \Proj_{0'} \bR.\]  
  \begin{corollary}
    \label{cor:Link-1}
    Let $\bA_0$, $\bA_1$, $\dots$, $\bA_{n-1}$ be algebras in a locally finite Taylor variety.
    Suppose $\bA_0$ is simple nonabelian and $\bA_1, \bA_2, \dots, \bA_{n-1}$ are abelian.
    If $\bR \sdp \myprod_{\nn} \bA_i$, then $\bR \sdp \bA_0 \times \bR_{0'}$ is linked.
  \end{corollary}
  \begin{proof}
    Suppose $\bR \sdp \bA_0 \times \bR_{0'}$ is not linked: $\etaR_0 \join \etaR_{0'} < 1_R$.
    Since $\bA_0$ is simple, $\etaR_0$ is a coatom, so
    $\etaR_0 \geq \etaR_{0'}$. Therefore, $ \etaR_{0'} = \etaR_0 \meet \etaR_{0'} = 0_R$,
    so $\bR \cong \bR/\etaR_{0'} \cong \bR_{0'} \leq \myprod_{0'}\bA_i$.
    This proves that $\bR$ is abelian, and yet
    the quotient $\bR/\etaR_0 \cong \bA_0$ is nonabelian,
    which contradicts Remark~\ref{rem:abelian-quotients}.
  \end{proof}

  Suppose we add to the respective contexts of the last three results the hypothesis
  that the algebras live in an idempotent variety with a Taylor
  \ifthenelse{\boolean{footnotes}}{%
    term.\footnote{Equivalently, assume the variety is idempotent and has a
      nontrivial \malcev condition, equivalently, is idempotent and omits
      tame-congruence type 1.}
  }{term.} 
  As mentioned above, we refer to such varieties as ``Taylor varieties'' and we call the
  algebras they contain ``Taylor algebras.'' In this context, we can apply the
  Absorption Theorem (in combination with other results from above) to
  deduce the following:

  \begin{lemma}
    \label{lem:rect-two_factors}
    Let $\bA_0$ and $\bA_1$ be finite  algebras in a Taylor variety
    with minimal absorbing subalgebras $\bB_i\minabsorbing \bA_i$ ($i =0,1$),
    and suppose $\bR \sdp \bA_0 \times \bA_1$, where $\etaR_0 \neq \etaR_1$.
    \begin{enumerate}[(i)] %label=(\roman*)]
    \item If $\bA_0$ and $\bA_1$ are simple and $R\cap (B_0 \times B_1) \neq \emptyset$, then
      $\bB_0 \times \bB_1\leq \bR$. 
    \item  If $\bA_0$ is simple nonabelian and $\bA_1$ is abelian,
      then $\bB_0 \times \bA_1 \leq \bR$.
    \end{enumerate}
  \end{lemma}
    \begin{proof}
      \begin{enumerate}[(i)] %[label=(\roman*)]
      \item First note that, by Corollary~\ref{cor:rect-two_factors-pre}, $\bR$ is linked.
        Let $\bR':= \bR \cap (\bB_0 \times \bB_1)$. Then by
        Lemma~\ref{lem:sdp-general} $\bR' \sdp \bB_0 \times \bB_1$,
        and by Lemma~\ref{lem:gen-abs1} $\bR'\absorbing \bR$, so $\bR'$ is also
        linked, by Lemma~\ref{lem:linked-absorber}.  The hypotheses of the
        Absorption Theorem---with $\bR'$ in place of $\bR$ and $\bB_i$
        in place of $\bA_i$---are now satisfied.  Therefore, 
        $\bR' = \bB_0 \times \bB_1$.
      \item This follows directly from Corollary~\ref{cor:S-NA-AF_A-pre} and the
        Absorption Theorem.
      \end{enumerate}
    \end{proof}

  Once again, by collecting multiple abelian factors into a single factor, we obtain
  \begin{corollary}
  \label{cor:S-NA-AF_A-multi}
  Let
  $\bA_0$, $\bA_1$, $\dots$, $\bA_{n-1}$
  be finite Taylor algebras, where
  $\bA_0$ is simple nonabelian, $\bB_0 \minabsorbing \bA_0$, 
  and the remaining $\bA_i$ are abelian, % (hence absorption-free)
  and suppose $\bR \sdp \myprod_{\nn}\bA_i$.
  Then $\bB_0 \times \bR_{0'} \leq \bR$.
  \end{corollary}
  \begin{proof}
    Obviously, $\bR \sdp \bA_0 \times \bR_{0'}$ and 
    $R \cap (B_0 \times R_{0'}) \neq \emptyset$.
    Also, $\bR_{0'}:=\Proj_{0'} \bR$ is abelian, so we can
    apply Lemma~\ref{lem:rect-two_factors} (ii), with $\bR_{0'}$ in place of $\bA_1$.
  \end{proof}
  

