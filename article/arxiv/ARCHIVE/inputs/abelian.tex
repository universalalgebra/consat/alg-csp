
In later sections nonabelian algebras will play the following role:
some of the theorems will begin with the assumption 
that a particular algebra $\bA$ is nonabelian and then proceed to show that 
if the result to be proved were false, then $\bA$ would have to be abelian. 
To prepare the way for such arguments, we review some basic facts about abelian
algebras. 


\subsection{Definitions}
Let $\bA = \<A, F^{\bA}\>$ be an algebra.
A reflexive, symmetric, compatible binary relation $T\subseteq A^2$ is called a
\defn{tolerance of $\bA$}.  
Given a pair $(\bu, \bv) \in A^m\times A^m$ of $m$-tuples of $A$, we write 
$\bu \mathrel{\bT} \bv$ just in case $\bu(i) \mathrel{T} \bv(i)$ for all $i\in \mm$. 
We state a number of definitions in this section using tolerance relations, but 
the definitions don't change when the tolerance in question happens to be
a congruence relation (i.e., a transitive tolerance).

Suppose $S$ and $T$ are tolerances on $\bA$.  An \defn{$S,T$-matrix} 
is a $2\times 2$ array of the form
\[
\begin{bmatrix*}[r] t(\ba,\bu) & t(\ba,\bv)\\ t(\bb,\bu)&t(\bb,\bv)\end{bmatrix*},
\]
where $t$, $\ba$, $\bb$, $\bu$, $\bv$ have the following properties:
\begin{enumerate}[(i)] %label=(\roman*)]
\item $t\in \sansClo_{\ell + m}(\bA)$,
\item $(\ba, \bb)\in A^\ell\times A^\ell$ and $\ba \mathrel{\bS} \bb$,
\item $(\bu, \bv)\in A^m\times A^m$ and $\bu \mathrel{\bT} \bv$.
\end{enumerate}
Let $\delta$ be a congruence relation of $\bA$.
If the entries of every $S,T$-matrix satisfy
\begin{equation}
  \label{eq:22}
t(\ba,\bu) \mathrel{\delta} t(\ba,\bv)\quad \iff \quad t(\bb,\bu) \mathrel{\delta} t(\bb,\bv),
\end{equation}
then we say that $S$ \defn{centralizes $T$ modulo} $\delta$ and we write 
$\CC{S}{T}{\delta}$.
That is, $\CC{S}{T}{\delta}$  means that 
(\ref{eq:22}) holds \emph{for all}
$\ell$, $m$, $t$, $\ba$, $\bb$, $\bu$, $\bv$ satisfying properties (i)--(iii).

The \defn{commutator} of $S$ and $T$, denoted by $[S, T]$,
is the least congruence $\delta$ such that $\CC{S}{T}{\delta}$ 
holds.  
Note that $\CC{S}{T}{0_A}$ is equivalent to $[S,T] = 0_A$, and this
is sometimes called the \defn{$S, T$-term condition};
when it holds we say  that
$S$ \defn{centralizes} $T$, and write $\C{S}{T}$.
A tolerance $T$ is called \defn{abelian} if
$\C{T}{T}$ (i.e., $[T, T] = 0_A$).  
An algebra $\bA$ is called \defn{abelian} if $1_A$ is abelian
(i.e., $\C{1_A}{1_A}$).
%% The \defn{centralizer of $\bT$ modulo $\delta$}, denoted by
%% $(\delta : \bT )$, is the largest congruence $\alpha$ on $\bA$ such that 
%% $\sansC(\alpha, \bT ; \delta)$ holds.

%% \ifthenelse{\boolean{extralong}}{%
%% \begin{remark}
%% An algebra $\bA$ is abelian iff $\C{1_A}{1_A}$ iff
%% \[
%% \forall \ell \in \{0,1,2,\dots \}, 
%% \quad \forall m \in  \{1,2,\dots \},
%% \quad \forall t\in \sansClo_{\ell + m}(\bA),
%% \quad \forall (a, b)\in A^\ell\times A^\ell,
%% \]
%% \[
%% \ker t(a, \cdot)=\ker t(b, \cdot).
%% \]
%% \end{remark}
%% }{}

%% \ifthenelse{\boolean{arxiv}}{%
%%   % wjd 2016.11.08: omitting remark from journal version
%%   \begin{remark}
%%     An algebra $\bA$ is abelian iff $\C{1_A}{1_A}$ iff
%%     \[
%%     \forall \ell, m \in \N,
%%     \quad \forall t\in \sansClo_{\ell + m}(\bA),
%%     \quad \forall (\ba, \bb)\in A^\ell\times A^\ell,
%%     \]
%%     \[
%%     \ker t(\ba, \cdot)=\ker t(\bb, \cdot).
%%     \]
%%   \end{remark}
%% }{}

\subsection{Facts about centralizers and abelian congruences}
We now collect some useful facts about centralizers of congruence relations
that are needed in Section~\ref{sec:tayl-vari-rect}.
The facts collected in the first lemma are well-known and easy to prove.
(For examples, see \cite[Prop~3.4]{HM:1988} and~\cite[Thm~2.19]{MR3076179}.)
\begin{lemma}
\label{lem:centralizers}
Let $\bA$ be an algebra and suppose
$\bB$ is a subalgebra of $\bA$. 
Let $\alpha$, $\beta$, $\gamma$, $\delta$, $\alpha_i$
$\beta_j$, $\gamma_k$
be congruences of $\bA$, for some 
$i \in I$, $j\in J$, $k \in K$. Then the following hold:
\begin{enumerate}
\item \label{centralizing_over_meet}
  $\CC{\alpha}{\beta}{\alpha \meet \beta}$;
\item \label{centralizing_over_meet2}
  if $\CC{\alpha}{ \beta}{ \gamma_k}$ for all $k \in K$, then
  $\CC{\alpha}{ \beta}{ \Meet_{K}\gamma_k}$;
\item \label{centralizing_over_join1}
  if $\CC{\alpha_i}{ \beta}{ \gamma}$ for all $i\in I$, then
  $\CC{\Join_{I}\alpha_i}{ \beta}{\gamma}$;
\item \label{monotone_centralizers1}
  if $\CC{\alpha}{ \beta}{ \gamma}$ and $\alpha' \leq \alpha$, then 
  $\CC{\alpha'}{ \beta}{ \gamma}$;
\item \label{monotone_centralizers2}
  if $\CC{\alpha}{ \beta}{ \gamma}$ and $\beta' \leq \beta$, then
  $\CC{\alpha}{ \beta'}{ \gamma}$;
\item \label{centralizing_over_subalg}
  if $\CC{\alpha}{ \beta}{ \gamma}$ in $\bA$, 
  then $\CC{\alpha\cap B^2}{ \beta\cap B^2}{\gamma\cap B^2}$ in $\bB$;
\item \label{centralizing_factors}
  if $\gamma \leq \delta$, then $\CC{\alpha}{ \beta}{ \delta}$
  in $\bA$ if and only if $\CC{\alpha/\gamma}{ \beta/\gamma}{ \delta/\gamma}$
  in $\bA/\gamma$.
\end{enumerate}
\end{lemma}


\ifthenelse{\boolean{extralong}}{% seems we never use this; omit from all but extralong version.
\begin{remark}
By (\ref{centralizing_over_meet}), 
if $\alpha \meet \beta = 0_{A}$,  
then $\C{\beta}{\alpha}$ and $\C{\alpha}{\beta}$.
\end{remark}
}{}

The next two lemmas are essential in a number proofs below.
The first lemma identifies special conditions
under which certain quotient congruences are abelian.
The second gives fairly general conditions under which
quotients of abelian congruences are abelian.
\begin{lemma}
  \label{lem:common-meets}
  Let $\alpha_0$, $\alpha_1$, $\beta$ be congruences of $\bA$ and suppose 
  $\alpha_0 \meet \beta = \delta = \alpha_1 \meet \beta$.
  Then $\CC{\alpha_0 \join \alpha_1}{ \beta}{ \delta}$.  If, in addition, 
  $\beta \leq \alpha_0 \join \alpha_1$, then 
  $\CC{\beta}{ \beta}{ \delta}$, so $\beta/\delta$ is an 
  abelian congruence of $\bA/\delta$.
\end{lemma}
Lemma~\ref{lem:common-meets}
is an easy consequence
of items (\ref{centralizing_over_meet}), (\ref{centralizing_over_join1}),
(\ref{monotone_centralizers1}), and (\ref{centralizing_factors}) of
  Lemma~\ref{lem:centralizers}.
\begin{lemma}
  \label{lem:abelian-quotients}
  Let $\var{V}$ be a locally finite variety with a Taylor term and let $\bA\in \var{V}$.
  Then $\CC{\beta}{\beta}{\gamma}$ for all $[\beta, \beta] \leq \gamma$.
\end{lemma}
Lemma~\ref{lem:abelian-quotients} can be proved  by combining  
the next result, of David Hobby and Ralph McKenzie,
with a result of Keith Kearnes and Emil Kiss.
\begin{lemma}[cf.~\protect{\cite[Thm~7.12]{HM:1988}}]
  \label{lem:HM-thm-7-12}
  A locally finite variety $\var{V}$ has a Taylor term if and only if it has a
  so called \defn{weak difference term}; that is, a term $d(x,y,z)$ satisfying
  the following conditions for all $\bA \in \var{V}$, all $a, b \in A$, and all
  $\beta \in \Con (\bA)$: 
  $d^{\bA}(a,a,b) \mathrel{[\beta, \beta]} b \mathrel{[\beta, \beta]} d^{\bA}(b,a,a)$,
  where $\beta = \Cg^{\bA}(a,b)$.
\end{lemma}

\begin{lemma}[\protect{\cite[Lem~6.8]{MR3076179}}]
  \label{lem:KK-lem-6-8}
  If $\bA$ belongs to a variety with a
  weak difference term and if $\beta$ and $\gamma$ are congruences of $\bA$
  satisfying $[\beta, \beta] \leq \gamma$, then $\CC{\beta}{\beta}{\gamma}$.
\end{lemma}
\begin{remark}
  \label{rem:abelian-quotients}
  It follows immediately from Lemma~\ref{lem:abelian-quotients} that in a locally
  finite Taylor variety, $\var{V}$, quotients of abelian algebras are abelian, so the
  abelian members of $\var{V}$ form a subvariety.
  But this can also be derived from Lemma~\ref{lem:HM-thm-7-12},
  since $[\beta, \beta] = 0_A$ implies $d^{\bA}$ is a \malcev term operation on
  the blocks of $\beta$, so if $\bA$ is abelian---i.e., if
  $\CC{1_A}{1_A}{0_A}$---then Lemma~\ref{lem:HM-thm-7-12},
  implies that $\bA$ has a \malcev term operation.
  (This will be recorded below in~Theorem~\ref{thm:type2cp}.)
  It then follows that homomorphic images of $\bA$ are
  abelian. (See~\cite[Cor~7.28]{MR2839398} for more details). 
\end{remark}


