
In this section we develop some useful notation for taking an instance of 
a \csp and restricting or reducing it in various ways,
either by removing variables or by reducing modulo a sequence of congruence relations. 
The utility of these tools will be demonstrated in Section~\ref{sec:csps-comm-idemp}.

Throughout this section, $\bA$ will denote a finite idempotent algebra.
The problem we will focus on is $\CSP(\mathfrak A)$, defined in
Section~\ref{sec:defin-constr-satisf}, and we will be particularly interested
in the special case in which $\mathfrak A = \sansS(\bA)$.

Recall, we denote an $n$-variable instance of $\CSP(\sansS(\bA))$ by
$\sI = \< \nn, \sA, \mathcal C\>$, where $\nn = \{0, 1, \dots, n-1\}$
represents the set of variables,
$\sA = (\bA_0, \bA_1, \dots, \bA_{n-1})$ % \in \sansS(\bA)^n$
is a list of $n$ subalgebras of $\bA$, and
$\sC = ((\sigma_0, R_0), (\sigma_1, R_1), \dots, (\sigma_{J-1}, R_{J-1}))$
is a list of constraints with respective arities $\ar(\sigma_j) = m_j$.
Thus, $R_j\subseteq \prod_{i < m_j} A_{\sigma(i)}$.
Much of the discussion below refers to an arbitrary constraint
in $\mathcal C$. In such cases it will simplify notation 
to drop subscripts and denote the constraint by $(\sigma, R)$.

\subsection{Variable reductions}
\subsubsection{Partial scopes and partial constraints}
Consider the restriction of an
$n$-variable \csp instance $\sI$ to the first $k$ of its variables, for some
$k\leq n$.
To start, we restrict an arbitrary scope $\sigma$ to the first $k$
variables. This results in a new \emph{partial scope} given by the function
\newcommand\kcapsigma{\ensuremath{\kk \, \cap\, \im \sigma}}
$\restr{\sigma}{\sigma^{-1}(\kcapsigma)}$.
Call this the \emph{$k$-partial scope of} $\sigma$ and, 
to simplify the notation, let
\[
\restr{\sigma}{\overleftarrow{\kk}}=\restr{\sigma}{\sigma^{-1}(\kcapsigma)}.
\]
If $\kcapsigma = \emptyset$ then the $k$-partial scope of $\sigma$
is the empty function.
To make the notation easier to digest, we give a small example below, but first
let's consider how to restrict a constraint to the first $k$ variables.
To obtain the \emph{$k$-partial constraint of} $(\sigma, R)$, we take the $k$-partial
scope of $\sigma$ as the new scope; for the constraint relation we take the
restriction of each tuple in $R$ to its first $p = |\kcapsigma|$ coordinates.  If we let
\[
\restr{R}{\overleftarrow{\kk}}=\restr{R}{\sigma^{-1}(\kcapsigma)},
\]
then the \emph{$k$-partial constraint} of $(\sigma, R)$ is given by
$(\restr{\sigma}{\overleftarrow{\kk}}, \restr{R}{\overleftarrow{\kk}})$.
The constraint relation $\restr{R}{\overleftarrow{\kk}}$
consists of all $p$-element initial segments of the tuples in $R$.
For example, suppose $\sigma$ is a scope consisting of the variables $2$, $4$, and $7$; that is,
$\sigma$ corresponds to the list $(\sigma(0), \sigma(1),\sigma(2)) = (2,4,7)$.
To find, say, the 5-partial constraint of $(\sigma, R)$, restrict $(\sigma, R)$
to the first $k= 5$ variables of the instance.
We have $\kk = \{0,1,2,3,4\}$ 
  \ifthenelse{\boolean{footnotes}}{%
    and\footnote{Without loss of generality,
      we assume all scope functions are injective, 
      so $\sigma^{-1}$ is well defined on $\im \sigma$.}
  }{and}
\[
\sigma^{-1}(\kcapsigma) = \sigma^{-1}(\{0,1,2,3,4\} \cap \{2,4,7\})
= \sigma^{-1}\{2,4\} = \{0,1\}.
\]
Therefore,
$\restr{\sigma}{\overleftarrow{\kk}} = (\sigma(0), \sigma(1)) = (2,4)$,
and
$\restr{R}{\overleftarrow{\kk}}$
consists of the initial pairs of the triples in $R$, that is,
$\{(x,y) \mid (x,y,z) \in R\}$.

\subsubsection{Partial instances}
The \emph{$k$-partial instance} of $\sI$ is the restriction of
$\sI$ to its first $k$ variables.  We will denote this partial instance
by $\sI_{\kk}$. Thus, $\sI_{\kk}$ is the instance with constraint set
$\mathcal{C}_{\kk}$ equal to the set of all $k$-partial constraints of $\sI$.
If we let $\Sol(\sI, \kk)$ denote the set of solutions to $\sI_{\kk}$,
then $f \in  \Sol(\sI, \kk)$ means that for each $j \in J$,
\[
\restr{\sigma_j}{\overleftarrow{\kk}}\; \in\;
\restr{R_j}{\overleftarrow{\kk}}. % {\sigma_j^{-1}(\kk\cap \im \sigma_j)}.
\]
We might be tempted to call $\Sol(\sI, \kk)$ a set of ``partial solutions,''
but that's a bit misleading since an $f \in \Sol(\sI, \kk)$ may or may not
extend to a solution to the full instance $\sI$.

\subsection{Quotient reductions}
As usual, let $\bA$ be a finite idempotent algebra, let
$\nn = \{0, 1, \dots, n-1\}$, and let
$\sI = \< \nn, \sA, \mathcal C\>$
denote an $n$-variable instance of $\CSP(\sansS(\bA))$.
As above, we assume
$\sA = (\bA_0, \bA_1, \dots, \bA_{n-1}) \in \sansS(\bA)^n$,
and $\sC = ((\sigma_0, R_0), (\sigma_1, R_1), \dots, (\sigma_{J-1}, R_{J-1}))$,
and $\bR_j\sdp \prod_{\mm_j} A_{\sigma(i)}$, for $0\leq j < J$.

\subsubsection{Quotient instances}
Suppose $\theta_i \in \Con (\bA_i)$ for each $0\leq i < n$ and define
\[
\btheta = (\theta_0, \theta_1, \dots, \theta_{n-1}) \in
\Con (\bA_0) \times \Con (\bA_1) \times \cdots \times \Con (\bA_{n-1}).
\]
If $\bx = (x_0, x_1, \dots, x_{n-1}) \in \prod_{\nn} A_i$, then denote
by $\bx/\btheta$ the tuple whose $i$-th component is the $\theta_i$-class of $\bA_i$
that contains $x_i$, so 
\[
\bx/\btheta = (x_0/\theta_0, x_1/\theta_1, \dots, x_{n-1}/\theta_{n-1}) \in
\prod_{i \in \nn} A_i/\theta_i.
\]
We need slightly more general notation than this since our tuples will often 
come from
$\prod_{\mm} A_{\sigma(i)}$ for some scope function
$\sigma\colon \mm \to \nn$.  If we view a general tuple $\bx \in \prod_{\nn} A_i$
as a function from $\nn$ to $\bigcup A_i$, and $\btheta$ as a function from
$\nn$ to $\bigcup \Con (\bA_i)$, then we write
$\bx \circ \sigma \in \prod_{\mm} A_{\sigma(i)}$ and
$\btheta \circ \sigma \in \prod_{\mm} \Con (\bA_{\sigma(i)})$ for the corresponding
scope-restricted
\ifthenelse{\boolean{footnotes}}{%
  tuples,\footnote{Here $\circ$ is function composition, not the relational product.}
}{tuples,}
and define
\[
(\bx \circ \sigma)/(\btheta \circ \sigma) =
(x_{\sigma(0)}/\theta_{\sigma(0)}, x_{\sigma(1)}/\theta_{\sigma(1)}, \dots, x_{\sigma(m-1)}/\theta_{\sigma(m-1)})
%% \in  \prod_{i\in \im \sigma} A_i/\theta_i.
\in  \prod_{i\in \mm} A_{\sigma(i)}/\theta_{\sigma(i)}.
\]
Given
$\btheta \in \prod_{\nn}\Con (\bA_i)$ and
a constraint $(\sigma, R) \in \mathcal C$ of $\sI$, define the
\emph{quotient constraint} $(\sigma, R/\btheta)$, where
\begin{equation}
  \label{eq:5}
  R/\btheta := \bigl\{\br/(\btheta\circ \sigma)
  \in \prod_{i \in \mm} A_{\sigma(i)}/\theta_{\sigma(i)} \mid \br \in R\bigr\}.
\end{equation}
Define the \emph{quotient instance} $\sI/\btheta$ to be the $n$-variable instance
of $\CSP(\sansH \sansS (\bA))$ with constraint set
\begin{equation}
  \label{eq:100}
  \mathcal{C}/\btheta:=\{(\sigma, R/\btheta)\mid (\sigma, R) \in \mathcal{C}\}.
\end{equation}
Here are a few easily proved facts about quotient instances.
\begin{fact}
  \label{fact:quotient-sdp}
  If $\btheta \in \prod_{\nn}\Con (\bA_i)$ and if
  $\bR \sdp \prod_{\mm} \bA_{\sigma(i)}$, then $R/\btheta$ defined in (\ref{eq:5})
  is a subuniverse of $\prod_{\mm}\bA_{\sigma(i)}/\theta_{\sigma(i)}$
  and the corresponding subalgebra is subdirect.
\end{fact}

\begin{fact}
  \label{fact:quotient-instance}
  If $\btheta \in \prod_{\nn}\Con (\bA_i)$ and if
  $\sI$ is an $n$-variable instance of $\CSP(\bA)$, then
  the constraint set $\mathcal C/\btheta$ defined in~(\ref{eq:100})
  defines an $n$-variable instance of $\CSP(\sansH \sansS (\bA))$.
\end{fact}

\begin{fact}
  \label{fact:soln-quotient}
  If $\bx$ is a solution to $\sI$, then $\bx/\btheta$ is a solution to $\sI/\btheta$.
\end{fact}
\noindent By Fact~\ref{fact:soln-quotient}, if there is a
quotient instance with no solution, then $\sI$ has no solution. However, the converse is false;
that is, there may be solutions to every proper quotient instance but no solution to the original instance.

\subsubsection{Block instances}
\label{sec:block-inst}
Let $\mathfrak A$ be a collection of finite idempotent algebras.
Let $\sI = \< \nn, \sA, \mathcal C\>$
be an $n$-variable instance of $\CSP(\mathfrak A)$,
where $\sA = (\bA_0, \bA_1, \dots, \bA_{n-1})$ and 
$\mathcal C$ is a finite set of constraints.
If $\btheta \in \prod_{\nn}\Con (\bA_i)$ and  
$\bx = (x_0, x_1, \dots, x_{n-1}) \in \prod_{\nn}A_i$, then by idempotence,
the list of blocks
$\bx/\btheta = (x_0/\theta_0, x_1/\theta_1, \dots, x_{n-1}/\theta_{n-1})$
is actually a list of algebras.
For each constraint $(\sigma, R)\in \mathcal C$, consider
restricting the relation $R$ to the $x_i/\theta_i$-classes in its scope $\sigma$.
In other words, 
replace the constraint $(\sigma, R)$ with the \emph{block constraint}
$(\sigma, R \cap \Pi_{\sigma} \bx/\btheta)$, where we have defined
\begin{equation}
  \label{eq:theta-block-algebras-notation}
\Pi_{\sigma} \bx/\btheta:= \prod_{i \in \mm} x_{\sigma(i)}/\theta_{\sigma(i)}.
\end{equation}
Finally, let $\sI_{\bx/\btheta} = \< \nn, \bx/\btheta, \sC_{\bx/\btheta}\>$
denote the problem instance of
$\CSP(\sansS(\sA))$
specified by the constraint set
\[
\mathcal{C}_{\bx/\btheta} := \bigl\{
(\sigma, R \cap \Pi_{\sigma}\bx/\btheta)
\mid
(\sigma, R) \in \mathcal{C}\bigr\}.
\]
We call $\sI_{\bx/\btheta}$ the \emph{$\bx/\btheta$-block instance of} $\sI$.
It is obvious that a solution to $\sI_{\bx/\btheta}$ is also a solution to $\sI$.



The notions ``quotient instance'' and ``block instance'' suggest a
strategy for solving \csps that works in
certain special cases (one of which we will see in Example~\ref{ex:quot-inst}).
First search for a solution $\bx/\btheta$
to the quotient instance $\sI/\btheta$ for some conveniently chosen $\btheta$.
If no quotient solution exists, then the original instance $\sI$ has no solution.
Otherwise, if $\bx/\btheta$ is a solution to $\sI/\btheta$,
then we try to solve the $\bx/\btheta$-block instance of $\sI$.
If we are successful, then the instance $\sI$ has a solution.
Otherwise, the strategy is inconclusive and we try again with a different
solution to $\sI/\btheta$.
After we have exhausted all solutions to the quotient instance, if we have still
not found a solution to any of the corresponding block instances,
then $\sI$ has no solution.

This approach is effective as long as we can find a quotient instance
$\sI/\theta$ for which there is a polynomial bound on the number of
quotient solutions.
Although this is not always possible (consider instances involving only simple algebras!),
there are situations in which an appropriate choice of congruences makes
it easy to check that every instance has quotient instance with a very small
number of solutions.  We present an example.

\begin{example}\label{ex:quot-inst}
Let $\bA = \<\{0,1,2,3\}, \cdot \>$, be an algebra with a single binary operation, `$\cdot $', given by the table on the left in Figure~\ref{tab:final-4}.
%% \begin{table}[h!]
\begin{figure}
\centering
 \begin{tabular}{c|cccc}
      $\cdot $ & 0 & 1 & 2 & 3\\
      \hline
      0 & 0 & 0 & 3& 2\\
      1 & 0 & 1 & 3& 2\\
      2 & 3 & 3 & 2 & 1\\
      3 & 2 & 2 & 1 & 3
 \end{tabular}
 \hskip1cm
  \begin{tabular}{c|ccc}
   $\cdot^{\Sq3}$&0&1&2\\
  \hline
  0&0&2&1\\
  1&2&1&0\\
  2&1&0&2
  \end{tabular}
 \hskip1cm
  \begin{tabular}{c|cccc}
      $t$ & 0 & 1 & 2 & 3\\
      \hline
      0 & 0 & 0 & 0& 0\\
      1 & 0 & 1 & 1& 1\\
      2 & 2 & 2 & 2 & 2\\
      3 & 3 & 3 & 3 & 3
    \end{tabular}
  \caption{Operation tables of Example~\ref{ex:quot-inst}:
    for the algebra $\bA$ (left);
    for the quotient $\bA/\Theta \cong \Sq3$ (middle); 
    for $t(x,y) = x\cdot (y\cdot (x\cdot y))$ (right).}
  \label{tab:final-4}
\end{figure}
%% \end{table}
The proper nonempty subuniverses are
$\{0,1\}$, $\{1,2,3\}$, and the singletons.
%% (in case we need it later, it's easy to prove that $\bA$ is absorption-free)
The algebra \bA\ has a single proper nontrivial congruence relation, $\Theta$, with partition $|01|2|3|$. 
The quotient algebra $\bA/\Theta$ is a 3-element Steiner quasigroup, which happens to be abelian.
We denote the latter by \Sq3 and give its binary operation
in the middle table in Figure~\ref{tab:final-4}.
Note that the subalgebra $\{1,2,3\}$ of $\bA$ is also isomorphic to $\Sq3$. 

As Theorem~\ref{thm:type2cp} predicts, the algebra $\bA/\Theta$ has a \malcev term $q(x,y,z)=y\cdot(x\cdot z)$. 
Let $s(x,y) = q(x,y,y) = y\cdot(x\cdot y)$. Then $\bA/\Theta\vDash s(x,y)\approx x$. By iterating the term $s$ on its second variable, we arrive at a term $t(x,y)$ such that, in~\bA, $t(x,t(x,y)) = t(x,y)$. In fact, $t(x,y)=x\cdot(y\cdot(x\cdot y))$. To summarize
\begin{align*}
\bA &\vDash t(x,t(x,y)) \approx t(x,y) \\
\bA/\Theta &\vDash t(x,y) \approx x.
\end{align*}
The table for $t$ appears on the right in Figure~\ref{tab:final-4}.

For the next result, we denote
the two-element semilattice
by $\slt=\<\{0,1\},\meet\>$.

\begin{lemma}\label{lem:sq3s2}
 Let $\mathfrak A = \{\slt,\Sq3\}$. Then $\CSP(\mathfrak A)$ is tractable.
 \end{lemma}

 \begin{proof}
 Let $\sI=\<\nn,\mathcal A, \mathcal C\>$ be an instance of $\CSP(\mathfrak A)$. 
 Recall, the set of solutions to $\sI$ is
 $\Sol(\sC, \nn) = \bigcap_{\sC} R^{\overleftarrow{\sigma}}$.
 We shall apply Corollary~\ref{cor:fry-pan} to establish 
 tractability.  
 We have $\mathcal A = (\bA_0, \bA_1,\dots,\bA_{n-1}) \in \mathfrak A^n$. Let 
 $\alpha=\{\,i : \bA_i \cong \Sq3\,\}$ and $\alpha'=\{\,i : \bA_i \cong \slt\,\}$, so 
 $\nn = \alpha \cup \alpha'$.  
 We may assume without loss of generality that, 
 for each constraint $(\sigma, R)$ appearing in $\mathcal C$, the associated algebra
 $\bR$ is a subdirect product of $\prod_{\mm}\bA_{\sigma(i)}$, where 
 $\bA_{\sigma(i)} \cong \Sq3$ for all $\sigma(i) \in \alpha$, and  
 $\bA_{\sigma(i)} \cong \slt$ for all $\sigma(i) \in \alpha'$.

 Let $0$ denote the bottom element of each semilattice.
 For each constraint $(\sigma, R)$,
 let $\restr{R}{\overleftarrow{\alpha}}$ be an abbreviation for 
 $\restr{R}{\sigma^{-1}(\alpha \cap \im \sigma)}$, which is the projection of
 $R$ onto the factors in its scope whose indices lie in $\alpha$.
 Let $\bzero$ denote a tuple of $0$'s of length $|\alpha' \cap \im \sigma|$.
 Then Corollary~\ref{cor:fry-pan} implies that
 for each constraint $(\sigma, R) \in \sC$, we have
 \begin{equation}
   \label{eq:fry-pan}
  \restr{R}{\overleftarrow{\alpha}} \times \{\bzero\} \subseteq R.
 \end{equation}
 Obviously, if $\sI$ has a solution, then the $\alpha$-partial instance
 $\sI_\alpha$ (i.e., the restriction of $\sI$ to the abelian factors) also has a solution.
 Conversely, if $f \in \prod_{\alpha}A_i$ is a solution to $\sI_\alpha$, then 
 (\ref{eq:fry-pan}) implies that $g \in \prod_{\nn} A_i$ defined by
 \[
   g(i) =
   \begin{cases}
     f(i), & \text{if $i \in \alpha$,} \\
     0, & \text{if $i \in \alpha'$}, 
   \end{cases}
 \]
 is a solution to $\sI$.
 We conclude that $\sI$ has a solution if and only if the $\alpha$-partial instance
 has a solution.  Since abelian algebras yield tractable \csps, the proof is complete.
\end{proof}

 Now we address the tractability of the example at hand. 
 
\begin{proposition}
  If $\<\bA, \cdot\>$ is the four-element \cib with operation table given in Figure~\ref{tab:final-4},
  then $\CSP(\sansS(\bA))$ is tractable.
\end{proposition}
\begin{proof}
  Let $\sI = \<\nn, \sA, \sC\>$ be an $n$-variable instance of $\CSP(\sansS(\bA))$
  specified by $\sA = (\bA_0, \bA_1, \dots, \bA_{n-1}) \in \sansS(\bA)^n$ and  
  $\mathcal C = ((\sigma_0, R_0), (\sigma_1, R_1), \dots, (\sigma_{J-1}, R_{J-1}))$.
  For each $i \in \nn$, let
  \begin{equation}
    \label{eq:90}
  \theta_i = 
  \begin{cases}
    \Theta, & \text{if $A_i = A$,}\\
    0_{A_i}, & \text{if $A_i \neq A$,}
  \end{cases}
  \end{equation}
  and define $\btheta = (\theta_0, \theta_1, \dots, \theta_{n-1})$.
  The universes $A_i/\theta_i$ of the quotient algebras defined above satisfy
  \begin{equation*}
  A_i/\theta_i = 
  \begin{cases}
    \{\{0,1\}, \{2\}, \{3\}\},  & \text{if $A_i = \{0,1,2,3\}$,}\\
    \{\{1\},\{2\},\{3\}\}, & \text{if $A_i = \{1,2,3\}$,}\\
    \{\{0\},\{1\}\}, & \text{if $A_i = \{0,1\}$.}
  \end{cases}
  \end{equation*}
  In the first two cases $\bA_i/\theta_i$ is isomorphic to $\Sq3$ and in the third case 
  $\bA_i/\theta_i$ is a 2-element meet semilattice.
  (Singleton factors, $\bA_i/\theta_i =\{a\}$, are ignored because all solutions
  must obviously assign the associated variable to the value $a$.)

  Consider the quotient instance $\sI/\btheta$. From the observations in the previous paragraph we see that $\sI/\btheta$ is an instance of $\CSP(\mathfrak A)$, where  $\mathfrak A  = \{\Sq3,\slt\}$. 
  By Lemma~\ref{lem:sq3s2}, $\sI/\btheta$ can be solved in polynomial-time.
  If $\sI/\btheta$ has no solution, then neither does $\sI$.  Otherwise, let
  $f  \in \Sol(\sI/\btheta, \nn)$ be
  a solution to the quotient instance.
    We will use $f$ to construct a solution $g \in \Sol(\sI, \nn)$ to the original
    instance.
  In doing so, we will occasionally map a singleton set to the element it contains; 
  observe that set union is such a map: $\bigcup\{x\} = x$.

  Assume $\nn = \alpha\cup \beta \cup \gamma$ is a disjoint union where
  \[ A_i = 
  \begin{cases}
    \{0,1,2,3\},  & \text{if $i \in \alpha$,}\\
    \{1,2,3\}, & \text{if $i \in \beta$,}\\
    \{0,1\}, & \text{if $i \in \gamma$.}
  \end{cases}
  \]
  
  In other words,
  $\prod_{\nn} A_i = \{0,1,2,3\}^\alpha \times \{1,2,3\}^\beta \times \{0,1\}^\gamma$.
  Then, according to definition (\ref{eq:90}),
  $\theta_i$ is $\Theta$ for $i \in \alpha$ and $0_{A_i}$ for $i\notin \alpha$.
  Thus, the $i$-th entry of $\sA/\btheta$ is
  \[
  A_i/\theta_i = 
  \begin{cases}
    \{\{0,1\}, \{2\}, \{3\}\},  & \text{if $i \in \alpha$,}\\
    \{\{1\},\{2\},\{3\}\}, & \text{if $i \in \beta$,}\\
    \{\{0\},\{1\}\}, & \text{if $i \in \gamma$.}
  \end{cases}
  \]
  The $i$-th value $f(i)$ of the quotient solution must belong to 
  $A_i/\theta_i$.
  %% $\{\{0,1\}, \{2\}, \{3\}\}$ if $0\leq i < k$,
  %% $\{1,2,3\}$ if $k\leq i < \ell$, and $\{0,1\}$ if $\ell \leq i < n$.
  Let $\alpha = \alpha_0 \cup \alpha_1$ be such that 
  $f(i) = \{0,1\}$ when $i \in \alpha_0$ and
  $f(i) \in\{ \{2\}, \{3\}\}$ when $i \in \alpha_1$. Then,
  \begin{equation}
    \label{eq:10}
  f \in \{\{0,1\}\}^{\alpha_0} \times \{\{2\}, \{3\}\}^{\alpha_1} \times
  \{\{1\},\{2\},\{3\}\}^{\beta} \times 
  \{\{0\},\{1\}\}^{\gamma}
  \end{equation}
  Note that $f(i)$ is a singleton for all $i \notin \alpha_0$, and recall that
  $\bigcup \{a\}= a$. Thus, the following defines an element of $\prod_{\nn}A_i$:
  \[
  g(i) = 
  \begin{cases}
    0, &  \text{ if $i \in \alpha_0\cup \gamma$,}\\ 
    \bigcup f(i), &  \text{ if $i\in \alpha_1 \cup \beta$.}
  \end{cases}
  \]
  We will prove that $g$ is a solution to the instance $\sI$; that is,
  $g\in \Sol(\sI, \nn)$. Recall, this holds if and only if $g \circ \sigma \in R$
  for each constraint $(\sigma, R) \in \sC$.
  
  Fix an arbitrary constraint $(\sigma, R)\in \sC$, let $S := \im \sigma$ be the set
  of indices in the scope of $R$, and let $S^c := \nn - S$ denote the complement
  of $S$ with respect to $\nn$. Since we assumed $f$ solves $\sI/\btheta$ and satisfies (\ref{eq:10})  
    there must exist $\br\in \prod_{\nn}A_i$ satisfying not only
  $(\sigma, R)$ but also the following: $\br(i) \in \{0,1\}$ for $i \in \alpha_0$ and 
  $\br(i) = \bigcup f(i)$ for $i \notin \alpha_0$. (Otherwise $f$
  wouldn't satisfy the quotient constraint $(\sigma, R/\btheta)$.)

  We now describe other elements satisfying the (arbitrary) constraint $(\sigma, R)$
  that we will use to prove $g$ is a solution.
  By subdirectness of $R$, for each $\ell \in \alpha_0 \cup \gamma$
  there exists $\bx^\ell \in \prod_{\nn}A_i$ satisfying $(\sigma, R)$ and $\bx^\ell(\ell) = 0$.
  Since $\br$ and $\bx^\ell$ satisfy $(\sigma, R)$---that
  is $\br \circ \sigma\in R$ and $\bx^\ell \circ \sigma\in R$---we also have
  $t(\br,  \bx^\ell) \circ \sigma\in R$.
  That is, $t(\br, \bx^\ell)$ satisfies the constraint $(\sigma, R)$.
  Let's compute the entries of
  $t(\br, \bx^\ell)$  using the partition $\nn = \alpha_0 \cup \alpha_1 \cup \beta \cup \gamma$ defined above.
  First, since $\bx^\ell(\ell) = 0$, we have $t(\br, \bx^\ell)(\ell)=0$.  For $i \neq \ell$, we observe the following:
  \begin{itemize}
  \item
    If $i\in \alpha_0 - \{\ell\}$, then $\br(i) \in \{0,1\}$, %% and $\bx^\ell(i) \in \{0,1,2,3\}$, 
    so $t(\br, \bx^\ell)(i)\in\{0,1\}$.
  \item
    If $i\in \alpha_1$, then $\br(i) = \bigcup f(i)\in \{2,3\}$, %% and $\bx^\ell(i) \in \{0,1,2,3\}$, 
    so $t(\br, \bx^\ell)(i)=\bigcup f(i)$.
  \item
    If $i\in \beta$, then $\br(i)= \bigcup f(i)\in \{1,2,3\}$, %% and $\bx^\ell(i) \in A$
    so $t(\br, \bx^\ell)(i) = \bigcup f(i)$.
  \item
    If $i\in \gamma-\{\ell\}$, then $\br(i)= \bigcup f(i)\in \{0,1\}$,  %% and $\bx^\ell(i) \in \{0,1\}$
    so $t(\br, \bx^\ell)(i) \in \{0,1\}$.
  \end{itemize}
  To summarize, for each $\ell \in \alpha_0 \cup \gamma$,
  there exists $t(\br, \bx^\ell) \in \prod_{\nn}A_i$
  satisfying $(\sigma, R)$ and having values
  $t(\br, \bx^\ell)(i) \in \{0,1\}$ for $i \in \alpha_0\cup \gamma$ and
  \[
  t(\br, \bx^\ell)(i) = 
  \begin{cases}
    0, & i = \ell,\\
    \bigcup f(i), & i\in \alpha_1 \cup \beta.
  \end{cases}
  \]
  Finally, if we take the product of all members of
  $\{t(\br, \bx^\ell) \mid \ell \in \alpha_0 \cup \gamma\}$
  with respect to the binary operation of the original algebra $\<A, \cdot\>$,
  then we find
  \[
  t(\br, \bx^{\ell_1}) \cdot t(\br, \bx^{\ell_2}) \cdot \cdots \cdot t(\br, \bx^{\ell_{L}}) = g,
  \]
  where $L = |\alpha_0\cup \gamma|$.
  Therefore, $g \in \Sol((\sigma, R), \nn)$. Since $(\sigma, R)$ was an arbitrary constraint,
  we have proved $g \in \Sol(\sI, \nn)$, as desired.
\end{proof}
\end{example}

%%
%%Least block algorithm. 
%%
\subsection{The least block algorithm}
This section describes an algorithm that was apparently discovered by
``some subset of'' Petar Markovi\'c and Ralph McKenzie.  We first learned about
this by reading Markovi\'c's slides~\cite{Markovic:2011}
from a 2011 talk in Krakow which gives a one-slide description of
the algorithm. Since no proof (or even formal statement of the assumptions) from the 
originators seems to be forthcoming, we include our own version and proof in this section. 

Let \bA\ be a finite idempotent algebra and let $\theta$ be a congruence on \bA. Recall that by 
idempotence, every $\theta$-class of $A$ will be a subalgebra of \bA. Suppose that, as an 
algebra, each $\theta$-class comes from a variety, $E$, possessing a $(k+1)$-ary edge term, 
$e$. Define $s(x,y)= e(y,y,x,x,\dots,x)$. Then according to Definition~\ref{defn:edge-term}, 
\begin{equation*}
E \vDash s(x,y) \approx x.
\end{equation*}
By iterating $s$ on its second variable, we obtain a binary term $t$ such that
\begin{align}
\bA &\vDash t(x,t(x,y)) \approx t(x,y)\label{eqn:ILT} \\
E &\vDash t(x,y) \approx x. \label{eqn:Lz}
\end{align}
Assume, finally, that the induced algebra $\<A/\theta, t^{\bA/\theta}\>$ is a linearly ordered 
meet semilattice. When this occurs, let us call \bA\ an \emph{Edge-by-Chain} (EC) algebra. 
Our objective in this subsection is to prove the following theorem.

\begin{theorem}\label{thm:least-block}
Every finite, idempotent EC algebra is tractable.
\end{theorem}

\begin{proof}
Recall that to call an algebra, \bA, tractable is to assert that the problem $\CSP(\sansS(\bA))$ 
is solvable in polynomial time. Let  \bA\ be finite, idempotent and EC. We continue to 
use the congruence $\theta$ and term $t$ developed above. Let $\sI$ be an instance of the 
problem $\CSP(\sansS(\bA))$. Thus $\sI$ is a triple $\<\sV,\sA,\sC\>$ in which 
$\sA=(\bA_0, \bA_1,\dots,\bA_{n-1})$ is a list of subalgebras of \bA, see Definition~
\ref{def:csp}.  The congruence $\theta$ induces a congruence (which we will continue to call $
\theta$) on each $\bA_i$, and the term $t$ will induce a linearly ordered semilattice structure 
on $A_i/\theta$. In other words, each $\bA_i$ is also an EC algebra.

Therefore, for each $i<n$, the structure of $\<A_i/\theta,t\>$ will be of the form $C_{i,0} < C_{i,
1} < \cdots < C_{i,q_i}$ in which each $C_{i,j}$ is a $\theta$-class of $\bA_i$. We have the 
following useful relationship.
\begin{equation}\label{eqn:almost-meet}
i<n,\, j\leq k,\, u\in C_{i,j},\, v\in C_{i,k} \implies   t(u,v) = u.
\end{equation}
To see this, let $w=t(u,v)$. Since the quotient structure is a semilattice under $t$, $w\in 
C_{i,j}$. Therefore, by \eqref{eqn:ILT}, $t(u,w)=t(u,t(u,v)) = t(u,v) = w$. On the other hand, 
since $u,w \in C_{i,j} \in E$ we have $t(u,w)=u$ by \eqref{eqn:Lz}. Thus $u=w$. 

 Recall that for an index $k\leq n$, the 
$k$th-partial instance of $\sI$ is the instance, $\sI_{\kk}$, obtained by
 restricting $\sI$ to the first $k$ variables  
(Section~\ref{sec:var-reduc}). 
The set of solutions to $\sI_{\kk}$ is denoted $\Sol(\sI,\kk)$.

For every $k < n$ we recursively define the index $j_k$ by
\begin{equation*}
j_k = \text{least  $j\leq q_k$ such that $\Sol(\sI,\kplus)\cap 
   \prod_{i=1}^k C_{i,j_i} \neq \emptyset$.}
\end{equation*}
If, for some $k$, no such $j$ exists, then $j_k$, $j_{k+1}$,\dots are undefined. The proof 
of the theorem follows from the following two assertions.
%% wjd (2017.02.12): Cliff, the new formatting below, which skips a line before
%%                   the first item, seems nicer to me (but feel free to change back if you wish)
%% \begin{claim}
%% \hangindent\leftmargini (1)%
%%  \hskip\labelsep $\sI$ has a solution if and only if $j_{n-1}$ is defined.
%% \begin{enumerate}\setcounter{enumi}{1}
%% \item $j_i$ can be computed in polynomial time for all $i<n$.
%% \end{enumerate}
%% \end{claim}
%%
%% wjd (2017.02.12): new formatting mentioned above.
\begin{claim}\
\begin{enumerate}
\item $\sI$ has a solution if and only if $j_{n-1}$ is defined.
\item $j_i$ can be computed in polynomial time for all $i<n$.
\end{enumerate}
\end{claim}

One direction of the first claim is immediate: if $j_{n-1}$ is defined then
$\Sol(\sI)= \Sol(\sI,\nn) 
$ is nonempty. We address the converse. Assume that $\sI$ has solutions. We argue, by 
induction on~$k$, that every $j_k$ is defined. For the base step, choose any $g\in \Sol(\sI)$.  
Since $g(0) \in A_0$, there is some $\ell$ such that $g(0) \in C_{0,\ell}$. Then $j_0$ is 
defined and is at most~$\ell$.

Now assume that $j_0, j_1,\dots,j_{k-1}$ are defined, but $j_k$ is undefined. Let $f\in \Sol(\sI,
\kk)\cap \prod_{i=0}^{k-1} C_{i,j_i}$. Since $j_k$ is undefined, $f$ has no extension to a 
member of $\Sol(\sI,\kplus)$. We shall derive a contradiction. Since $\sI$ has a solution, 
the restricted instance $\sI_{k+1}$ certainly has solutions. Choose any $g\in \Sol(\sI,k+1)$ with $g(k) 
\in C_{k,\ell}$ for the smallest possible~$\ell$. Let $\sC= \bigl((\sigma_0,R_0), 
(\sigma_1,R_1),\dots,(\sigma_{J-1},R_{J-1})\bigr)$ be the list of constraints of $\sI$. 
By assumption, for each $m<J$, $f$ is a solution to $\restr{(\sigma_m,R_m)}{\kk}$ (the 
restriction of $R_m$ to its first $k$ variables). Therefore, there is an
extension, $f_m$, of $f$, to $k+1$ variables such that $f_m$ is a solution to
$\restr{(\sigma_m,R_m)}{\kplus}$.  

Let $m<J$ and set $h_m = t(g,f_m)$. Since both $g$ and $f_m$ are
solutions to $\restr{(\sigma_m,R_m)}{\kplus}$, and $t$ is a term, $h_m$ is also
a solution to $\restr{(\sigma_m,R_m)}{\kplus}$.  
For every $i<k$ we have $f_m(i)= f(i) = f_0(i)$. Thus 
\begin{gather*}
h_m(i) = t\bigl(g(i), f_m(i)\bigr) = t\bigl(g(i),f_0(i)\bigr) = h_0(i), \text{ for $i<k$ and}\\
h_m(k) = t\bigl(g(k),f_m(k)\bigr) = g(k).
\end{gather*}
The latter relation holds by \eqref{eqn:almost-meet} 
and the choice of $g$. It follows that every $h_m$ coincides with $h_0$. Since $h_m$ satisfies 
$R_m$, we have $h_0 \in \Sol(\sI,\kplus)$. This contradicts our assumption that $f$ has no 
extension to a member of $\Sol(\sI,\kplus)$. 

Finally, we must verify the second claim. Assume we have computed $j_i$, for $i<k$. To 
compute $j_k$ we proceed as follows.
\begin{algorithmic}
 \For{$j=0$ to $q_k$}
 \State $\sB \leftarrow (\bC_{0,j_0}, \bC_{1,j_1}, \dots, \bC_{k-1,j_{k-1}},\bC_{k,j})$
 \If{$\sI_{\sB}$ has a solution in $\CSP(\sB)$}
 \Return $j$
 \EndIf
 \EndFor
 \Return \textsc{Failure}
 \end{algorithmic}
 In this algorithm, $\sI_{\sB}$ is the instance of $\CSP(\sB)$ obtained by first restricting $\sI$ to 
 its first $k+1$ variables, and then restricting each constraint relation to $\prod\sB$. Since the 
members of $\sB$ all lie in the edge-term variety $E$, it follows from 
Theorems~\ref{thm:edge-tractable} and~\ref{thm:HSP-tract} that $\CSP(\sB)$ runs in polynomial-time. 
 \end{proof}
