In this section we give a precise definition of what we mean by a ``constraint
satisfaction problem,'' and what it means for such a problem to be ``tractable.''
We then give some examples demonstrating how one uses the algebraic tools we
have developed to prove tractability.

We define a ``constraint satisfaction problem'' in a way that is convenient
for our purposes. This is not the most general definition possible, but for now
we postpone consideration of the scope of our setup.

Let $\bA = \<A, \sF\>$ be a finite idempotent algebra,
and let $\Sub(\bA)$ and $\sansS(\bA)$ denote the set of subuniverses and subalgebras
of $\bA$, respectively. 
\begin{definition}
  \label{def:csp}
  Let $\mathfrak{A}$ be a collection of algebras of the same similarity type.
  We define $\CSP(\mathfrak{A})$ to be the following decision problem:
\begin{itemize}
\item  An  \defn{$n$-variable instance} of $\CSP(\mathfrak{A})$
  is a quadruple $\<\sV, \sA, \sS, \sR\>$ consisting of
  \begin{itemize}
  \item a set $\sV$ of $n$ \emph{variables}; often we take $\sV$ to be
    $\nn= \{0, 1, \dots, n-1\}$;
  \item a list $\sA = (\bA_0, \bA_1, \dots, \bA_{n-1})\in \mfA^n$ of
    algebras from $\mathfrak{A}$, one for each variable;
  \item a list $\sS = (\sigma_0, \sigma_1, \dots, \sigma_{J-1})$
    of \emph{constraint scope functions}
    with arities $\ar(\sigma_j) = m_j$;
  \item a list $\sR = (R_0, R_1, \dots, R_{J-1})$ of \emph{constraint relations}, where
    each $R_j$ is the universe of a subdirect product of the algebras
    in $\sA$ with indices in $\im \sigma_j$; that is,
    \[\bR_j \sdp \prod_{0\leq i < m_j}\bA_{\sigma_j(i)}.\]
  \end{itemize}
\item A \defn{solution} to the instance $\<\sV, \sA, \sS, \sR\>$
  is an assignment %  $f \in \prod_{i\in \sV}A_i$ 
  $f \colon \sV \to \bigcup_{\nn}A_i$ %% $f: \sV \to A$
  of values to variables that satisfies all constraint relations.  More precisely,
  $f\in \myprod_{\nn}A_i$ and $f \circ \sigma_j \in R_j$ holds for all $0\leq j < J$,
  
\end{itemize}
\end{definition}
We will denote the set of solutions to the instance $\<\sV, \sA, \sS,\sR\>$ by $\Sol(\sS,\sR, \nn)$.

\begin{remark}\
  \begin{enumerate}[(i)] %[label=(\roman*)]
  \item The $i$-th scope function $\sigma_i \colon \mm_i \to \sV$ picks out the $m_i$
    variables from $\sV$ involved in the constraint relation $R_i$. Thus, the
    list $\sS$ of scope functions belongs to $\myprod_{i< J}\sV^{m_i}$.
  \item
    Frequently we require that the arities of the scope functions be bounded
    above, i.e., $\ar(\sigma_j) \leq m$, for all $j<J$. This gives rise to the
    \emph{local constraint satisfaction problem} $\CSP(\mathfrak A,m)$ consisting
    of instances of this restricted type.  
  \item
    If $(\sigma, R) \in \sC$ is a constraint of an $n$-variable instance
    %% $\<\sV, \sA, \sC\>$,
    then we denote by $\Sol((\sigma, R), \nn)$ the set of all tuples in $\prod_{\nn}A_i$
    that satisfy $(\sigma, R)$. 
    In fact, we already introduced the notation $R^{\overleftarrow{\sigma}}$ for this set
    in~(\ref{eq:19}) of Section~\ref{sec:proj-scop-kern}.  Recall, 
    $\bx \in R^{\overleftarrow{\sigma}}$ iff $\bx \circ \sigma \in R$. Thus, 
    \[
    \Sol((\sigma, R), \nn) = R^{\overleftarrow{\sigma}}
    := \bigl\{\bx \in \prod_{i\in \nn}A_i %\prod_{i\in \nn}A_i
    \mid \bx \circ \sigma \in R\bigr\}.
    \]
    Therefore, the set of solutions to the instance  $\<\sV, \sA, \sC\>$ is
    \[\Sol(\sC, \nn) = \bigcap_{(\sigma, R) \in \sC} R^{\overleftarrow{\sigma}}.\]

\item If $\mathfrak A$ contains a single algebra, we write $\CSP(\bA)$
    instead of $\CSP(\{\bA\})$.  It is important to note that, in our definition of an
    instance of $\CSP(\mathfrak A)$, a constraint relation is a subdirect product of
    algebras in $\mathfrak A$. This means that the constraint relations of an
    instance of $\CSP(\bA)$ are subdirect powers of $\bA$.
    In the literature it is conventional to allow constraint relations
    of $\CSP(\bA)$ to be subpowers of $\bA$. 
    Such interpretations would correspond to instances of
    $\CSP(\sansS (\bA))$  in our notation.
  \end{enumerate}
\end{remark}


\subsection{Instance size and tractability}
\label{sec:inst-size-tract}
We measure the computational complexity of an algorithm for solving instances
of $\CSP(\mathfrak A)$ as a function of input size. In order to do this, and to say
what it means for $\CSP(\mathfrak A)$ to be ``computationally tractable,'' we first
need a definition of input size. 
In our case, this amounts to determining the number of bits required to completely
specify an instance of the problem. In practice, an upper bound on the size is
usually sufficient.  

Using the notation in Definition~\ref{def:csp} as a guide, we bound the size of
an instance $\sI=\<\sV, \sA, \sS, \sR\>$ of $\CSP(\mathfrak A)$. Let
$q=\max(\card{A_0},\, \card{A_1},\dots,\card{A_{n-1}})$, let $r$ be the maximum
rank of an operation symbol in the similarity type, and $p$ the number of
operation symbols. Then each member of the list $\sA$ requires at most $pq^r\log
q$ bits to specify. Thus 
\begin{equation*}
\size(\sA) \leq npq^r\log q.
\end{equation*}
Similarly, each constraint scope $\sigma_j\colon \mm_j \to \nn$ can be encoded
using $m_j\log n$ bits. Taking $m=\max(m_1,\dots,m_{J-1})$ we have 
\begin{equation*}
\size(\sS) \leq Jm\log n.
\end{equation*}
Finally, the constraint relation $R_j$ requires at most $q^{m_j}\cdot m_j \cdot
\log q$ bits. Thus 
\begin{equation*}
\size(\sR) \leq Jq^m\cdot m\log q.
\end{equation*}
Combining these encodings and using the fact that $\log q \leq q$, we deduce that
\begin{equation}\label{eqn:size}
\size(\sI) \leq npq^{r+1} + Jmq^{m+1} + Jmn. 
\end{equation}
In particular, for the problem $\CSP(\mathfrak A,m)$, the parameter $m$ is
considered fixed, as is~$r$. 
In this case, we can assume $J\leq n^m$. Consequently $\size(\sI) \in O((nq)^{m+1})$ 
which yields a polynomial bound (in $nq$) for the size of the instance.

A problem is called \defn{tractable} if there exists a deterministic
polynomial-time algorithm solving all instances of that problem.
We can use Definition~\ref{def:csp} above to classify the complexity of an algebra
$\bA$, or collection of algebras $\mathfrak A$, according to the complexity of their
corresponding constraint satisfaction problems.

An algorithm $\sansA$ is called a \defn{polynomial-time algorithm}
for $\CSP(\mathfrak A)$ %(or \defn{runs in polynomial-time}
if there exist constants $c$ and $d$ such that, given an instance $\sI$ of
$\CSP(\mathfrak A)$ of size $S= \size(\sI)$,
$\sansA$ halts in at most $c S^d$ steps and outputs
whether or not $\sI$ has a solution.  In this case, we say $\sansA$
``solves the decision problem $\CSP(\mathfrak A)$ in polynomial time''
and we call the algebras in $\mathfrak A$ ``jointly tractable.''
Some authors say that an algebra $\bA$ as tractable when
$\mathfrak A = \sansS(\bA)$ is jointly tractable,
or when $\mathfrak A = \sansS \sansP_{\mathrm{fin}} (\bA)$
is jointly tractable.
We say that $\mathfrak A$ is \emph{jointly locally tractable} if, for every
natural number, $m$, there is a polynomial-time algorithm $\sansA_m$ that solves
$\CSP(\mathfrak A,m)$.  

We wish to emphasize that, as is typical in computational complexity, the
problem $\CSP(\mathfrak A)$ is a \emph{decision problem,} that is, the algorithm
is only required to respond ``yes'' or ``no'' to the question of whether a
particular instance has a solution, it does not have to actually produce a
solution. However, it is a surprising fact that if $\CSP(\mathfrak A)$ is
tractable then the corresponding \emph{search problem} is also tractable, in
other words, one can design the algorithm to find a solution in polynomial time,
if a solution exists,  
see~\cite[Cor~4.9]{MR2137072}.





\subsection{Sufficient conditions for tractability}
\label{ssec:edge-sdm}
A lattice is called \emph{meet semidistributive} if it satisfies the quasiidentity
\begin{equation*}
x\meet y \approx x\meet z \rightarrow x\meet y \approx x\meet (y\join z).
\end{equation*}
A variety is \sdm if every member algebra has a meet semidistributive
congruence lattice. Idempotent \sdm varieties are known to be
Taylor~\cite{HM:1988}. In~\cite{MR2893395}, Barto and Kozik proved the
following. 

\begin{theorem}\label{thm:sdm-tractable}
Let \bA\ be a finite idempotent algebra lying in an \sdm variety. Then \bA\ is tractable. 
\end{theorem}

A second significant technique for establishing tractability is the ``few
subpowers algorithm,'' which, according to its discoverers, is a broad
generalization of Gaussian elimination. 

\begin{definition}\label{defn:edge-term}
Let $\var{V}$ be a variety and $k$ an integer, $k>1$. A $(k+1)$-ary term $t$ is
called a \emph{$k$-edge term for $\var{V}$} if the following $k$ identities hold
in $\var{V}$: 
\begin{align*}
t(y,y,x,x,x,\dots,x) &\approx x\\
t(y,x,y,x,x,\dots,x) &\approx x\\
t(x,x,x,y,x,\dots,x) &\approx x\\
&\vdots\\
t(x,x,x,x,x,\dots,y) &\approx x.
\end{align*}
\end{definition}

Clearly every edge term is idempotent and Taylor. It is not hard to see that
every \malcev term and every near unanimity term is an edge term. Combining the
main results of \cite{MR2563736} and~\cite{MR2678065} yields the following
theorem. 

\begin{theorem}\label{thm:edge-tractable}
Let \bA\ be a finite idempotent algebra with an edge term. Then \bA\ is tractable. 
\end{theorem}

Finally, we comment that tractability is largely preserved by familiar algebraic constructions.

\begin{theorem}[\cite{MR2137072}]\label{thm:HSP-tract}
Let \bA\ be a finite, idempotent, tractable algebra. Every subalgebra and finite
power of \bA\ is tractable. Every homomorphic image of \bA\ is locally
tractable.  
\end{theorem}




























