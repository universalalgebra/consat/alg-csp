===============
Article 5263
===============

Referee's Comments
------------------------------

General Comments
~~~~~~~~~~~~~~~~~~~~~

The paper makes several valuable contributions to the algebraic theory of the fixed template Constraint Satisfaction Problems (CSPs).

The famous dichotomy conjecture of Feder and Vardi (The 2018 Alonzo Church Award) predicts that every fixed finite template CSP is either in P or NP-complete. A refinement of Bulatov, Jeavons, and Krokhin (SICOMP 2015) has predicted a precise borderline and proved the hardness part. On the algorithmic side, two basic polynomial algorithmic ideas exists and their applicability is fully understood. It was widely believed that all the tractable cases are solvable by some (complicated) combination of these two basic ideas. And indeed, the recent announcements of the solution of the dichotomy conjecture (Bulatov and Zhuk, independently, FOCS'17 best paper awards) both use such a combination.

The results presented in this paper predates the work of Bulatov and Zhuk and also combine (in a simpler way) the two algorithms. The contributions are:

* "Rectangularity Theorem". This theorem gives (under certain assumptions) a strong structural properties of relations which appear in the interesting instances of CSPs (that is, instances which were conjectured tractable by Bulatov, Jeavons, and Krokhin). This is perhaps the main mathematical contribution. It provides a tool to the algorithmic results of the paper and it is of independent mathematical interest. As authors discuss, the result was announced in 2015 by Barto and Kozik at a conference, but a full proof has not been published (and it is possible that it contains gaps as the discussion in the paper suggests). The authors provide an independent, full proof.

* Several techniques to combine the algorithms. Among them the "Chain over Maltsev" algorithm - another result which was announced by Markovic and McKenzie and was not published.

* The authors used the collected tools to prove that each algebra with at most 4 elements and a single binary commutative operation determines a tractable CSP. This is an interesting special case of the dichotomy conjecture.

The theorems of Bulatov and Zhuk do not make this paper obsolete: the presented results may contribute to a simpler proof, the results have a potential to be applied elsewhere (in particular, in universal algebra), and the paper can serve as a source of examples (both in CSP and universal algebra).

The presentation of the results is also very good and I have only minor comments listed below.

I recommend the paper be accepted to LMCS.

--------------------------------------------

Authors' Responses
------------------

The authors are very grateful for the referee's thorough review of the manuscript and for the many suggestions for improvements they provided.  We used the table below as a checklist to ensure that we had addressed each concern. As indicated in the table, we have adopted all but three of the referee's suggestions. The three exceptions are marked "UNCHANGED" and accompanied by justification in the right-hand column of the table.

Thus, the manuscript has been thoroughly revised, corrected, and improved. In particular, in order to correct the mistakes mentioned in the last four items in the list above, the proofs in the final section have been carefully reworked.

Finally, we have streamlined the entire paper, thereby reducing its length by about 8 pages.  We believe this results in a more efficient, readable, and less heavy-handed presentation.

+----+--------------------------------------------------------------------------------+--------------------------------------------+
| pg | Referee Comment                                                                |  Author Response                           |
+====+================================================================================+============================================+
|    | the intro & other places needs adjustments because of Bulatov and Zhuk Thm     | fixed (in particular, see note at end of   |
|    |                                                                                | introduction)                              |
|    |                                                                                |                                            |
+----+--------------------------------------------------------------------------------+--------------------------------------------+
|    | could be helpful to add comment about the condition :math:`\eta_i \neq \eta_j` | fixed (added new paragraph above Lem. 5.4) |
|    | (e.g. in Lemma 5.4) that the opposite means a redundant coordinate             |                                            |
|    |                                                                                |                                            |
|    |                                                                                |                                            |
|    |                                                                                |                                            |
+----+--------------------------------------------------------------------------------+--------------------------------------------+
| 2  | Conjecture 1.1, l1: Taylor term IN its clone                                   | fixed                                      |
+----+--------------------------------------------------------------------------------+--------------------------------------------+
| 2  | line above Thm 1.2: shortcut [FMar] for authors Freese, McKenzie seems strange | fixed (changed to [FM16])                  |
+----+--------------------------------------------------------------------------------+--------------------------------------------+
| 7  | sec4.1: The definition of linked includes subdirectness, but is used for       | fixed (changed to subsets of products;     |
|    | non-subdirect relations in, e.g., Lem. 4.11. I suggest to omit subdirectness   | however, we need :math:`Proj_0 R`, since   |
|    | requirement. (Also, as presented, it's a bit confusing to use :math:`Proj_0 R` | elements must be 1st coords of pairs in R, |
|    | instead of :math:`A_0` in the definition of linked and Fact 4.1.)              | not simply elements of :math:`A_0`)        |
|    |                                                                                |                                            |
|    |                                                                                |                                            |
|    |                                                                                |                                            |
+----+--------------------------------------------------------------------------------+--------------------------------------------+
| 9  | proof of Lemma 4.9: so the result follows FROM                                 | fixed                                      |
+----+--------------------------------------------------------------------------------+--------------------------------------------+
| 9  | 4 lines above Lemma 4.10: "If C is a subuniverse..." Add "and there is an      | fixed (omitted the offending sentence      |
|    | operation that depends on more than 1 argument"                                |                                            |
|    |                                                                                |                                            |
+----+--------------------------------------------------------------------------------+--------------------------------------------+
| 10 | below Corollary 4.15: correct the first sentence                               | fixed (paragraph rewritten)                |
+----+--------------------------------------------------------------------------------+--------------------------------------------+
| 10 | Cor.4.16 and elsewhere (eg. p34, l4): instead of "subdirect product of X x Y", | UNCHANGED (There's no difference, and      |
|    | shouldn't it rather be "subdirect product of X and Y"?                         | "subdirect product of X x Y" seems to us   |
|    |                                                                                | more natural and easier to read.           |
|    |                                                                                |                                            |
|    |                                                                                |                                            |
+----+--------------------------------------------------------------------------------+--------------------------------------------+
| 10 | proof of Cor.4.16, 2nd part: argue that the intersection is subdirect          | fixed                                      |
|    | Lemma 4.12 and mention here that 4.13 is also used. Also, the last but one     |                                            |
|    | line ":math:`B_0 \times A_1 \to A_0 x B_1`                                     |                                            |
|    |                                                                                |                                            |
|    |                                                                                |                                            |
+----+--------------------------------------------------------------------------------+--------------------------------------------+
| 11 | proof of Lemma 4.18 (ii). Refer to Lemma 4.13 as well                          | fixed                                      |
|    |                                                                                |                                            |
+----+--------------------------------------------------------------------------------+--------------------------------------------+
| 17 | item (iv): If the constraint relations are not assumed subdirect, then this    | fixed (and included a reference to         |
|    | does not directly correspond to CSP(S(A)) in your notation since projections   | arc-consistency algorithm)                 |
|    | of two constraints onto the same variables can be different (one needs to      |                                            |
|    | first impose arc-consistency). Please expand a bit.                            |                                            |
|    |                                                                                |                                            |
|    |                                                                                |                                            |
|    |                                                                                |                                            |
|    |                                                                                |                                            |
|    |                                                                                |                                            |
+----+--------------------------------------------------------------------------------+--------------------------------------------+
| 18 | Theorem 6.3 (and elsewhere): It is not defined what tractable means for an     | fixed (it was defined in the last          |
|    | algebra, please define it (it is actually first defined in the proof of        | paragraph on p. 17, but has been reworded  |
|    | Thm 7.7 if I am not overlooking something).                                    | for clarity and now appears in the last    |
|    |                                                                                | sentence of paragraph 2 on page 16)        |
|    |                                                                                |                                            |
|    |                                                                                |                                            |
|    |                                                                                |                                            |
|    |                                                                                |                                            |
|    |                                                                                |                                            |
+----+--------------------------------------------------------------------------------+--------------------------------------------+
| 19 | item (1): It is a bit confusing when it is said that "Cor 5.8. assumes ...     | fixed                                      |
|    | we can efficiently find a solution..." since Cor 5.8. does not talk about      |                                            |
|    | efficient computation at all. Please improve the discussion.                   |                                            |
|    |                                                                                |                                            |
|    |                                                                                |                                            |
|    |                                                                                |                                            |
|    |                                                                                |                                            |
+----+--------------------------------------------------------------------------------+--------------------------------------------+
| 20 | Prop 6.8 (and the sentence above): final claim in statement is incorrect---it  | fixed                                      |
|    | should be that one of the intersections is empty                               |                                            |
|    |                                                                                |                                            |
+----+--------------------------------------------------------------------------------+--------------------------------------------+
| 21 | above Fact 6.10: Please add refernce to paper where "transitive" is introduced | fixed                                      |
|    |                                                                                |                                            |
|    |                                                                                |                                            |
+----+--------------------------------------------------------------------------------+--------------------------------------------+
| 26 | paragraph above Ex 7.4: The discussion is a bit misleading since in the later  | UNCHANGED (The algorithm may have to       |
|    | situation it does not seem to be important that the quotient instance has      | check every solution to the quotient       |
|    | few solutions (rather, a single solution is used). Please improve. It could    | instance, especially if the original       |
|    | be also helpful to say that Ex. 7.4 will handle one of the cases in Section    | instance has no solution; so, if the       |
|    |                                                                                | quotient instance has exponentially many   |
|    |                                                                                | solutions, the algorithm is  intractable.) |
|    |                                                                                |                                            |
+----+--------------------------------------------------------------------------------+--------------------------------------------+
| 29 | Sec 7.3, 2nd paragraph, "from a variety, E, ....": varieties were previously   | fixed                                      |
|    | in a different font.                                                           |                                            |
|    |                                                                                |                                            |
+----+--------------------------------------------------------------------------------+--------------------------------------------+
| 30 | l1, "...semilattice under t, w ...": add "then" after the comma.               | fixed                                      |
|    |                                                                                |                                            |
|    |                                                                                |                                            |
+----+--------------------------------------------------------------------------------+--------------------------------------------+
| 30 | l8 (the displyed line): the whole "k+1" should be underlined                   | fixed                                      |
|    |                                                                                |                                            |
|    |                                                                                |                                            |
+----+--------------------------------------------------------------------------------+--------------------------------------------+
| 31 | above Thm 8.2: Perhaps mention that it is enough to check condition for CTB    | UNCHANGED (We don't believe this is        |
|    | for basic operations (since later this is what's done)                         | what's done in the paper.)                 |
|    |                                                                                |                                            |
|    |                                                                                |                                            |
|    |                                                                                |                                            |
+----+--------------------------------------------------------------------------------+--------------------------------------------+
| 32 | Cor 8.4, 2nd paragraph: Most readers will not be familiar with the notions     | fixed (added reference and definitions)    |
|    | "strongly irregular" and "Plonka sum". Please add a reference.                 |                                            |
|    |                                                                                |                                            |
|    |                                                                                |                                            |
|    |                                                                                |                                            |
+----+--------------------------------------------------------------------------------+--------------------------------------------+
| 33 | case n=3, A simple, "... there are 2 algebras": add "non-isomorphic"           | fixed                                      |
|    |                                                                                |                                            |
|    |                                                                                |                                            |
+----+--------------------------------------------------------------------------------+--------------------------------------------+
| 34 | l4,"Therefore, by 8.4....": add "Corollary"                                    | fixed                                      |
|    |                                                                                |                                            |
|    |                                                                                |                                            |
+----+--------------------------------------------------------------------------------+--------------------------------------------+
| 34 | above the case n=4, A simple: It is not quite clear why A is EC (the           | fixed                                      |
|    | definition talks about edge terms...). Please give more details.               |                                            |
|    |                                                                                |                                            |
|    |                                                                                |                                            |
|    |                                                                                |                                            |
+----+--------------------------------------------------------------------------------+--------------------------------------------+
| 35 | l4 (1st stence of 2nd paragraph): the absorbing term does not work for         | fixed (proof rewritten)                    |
|    | i=2 (x=0, y=3), one more iteration of the construction is needed.              |                                            |
|    |                                                                                |                                            |
|    |                                                                                |                                            |
|    |                                                                                |                                            |
+----+--------------------------------------------------------------------------------+--------------------------------------------+
| 35 | last but one paragraph: The way how redundant coordinates are removed does     | fixed (proof rewritten)                    |
|    | not work at all (for example, equality constraints will become void after      |                                            |
|    | the construction - this can clearly change the solvability of the instance)    |                                            |
|    |                                                                                |                                            |
|    |                                                                                |                                            |
|    |                                                                                |                                            |
|    |                                                                                |                                            |
+----+--------------------------------------------------------------------------------+--------------------------------------------+
| 36 | 2nd paragraph: {0} is not a sink of A by the definition - consider the basic   | fixed (proof rewritten)                    |
|    | operation. It is just a sink w.r.t. the operation t                            |                                            |
|    |                                                                                |                                            |
|    |                                                                                |                                            |
|    |                                                                                |                                            |
+----+--------------------------------------------------------------------------------+--------------------------------------------+
